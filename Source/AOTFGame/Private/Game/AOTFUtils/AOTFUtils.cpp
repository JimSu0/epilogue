// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AOTFUtils/AOTFUtils.h"

namespace AOTFUtils
{
	void FAOTFMath::CalcWithLimit(float& Value, float MinLimitValue, float MaxLimitValue, float DtValue)
	{
		if (Value <= MinLimitValue || Value >=MaxLimitValue)
		{
			return;
		}
		Value += DtValue;
	}
	
	void FAOTFMath::AddWithLimit(float& Value, float LimitValue, float AddValue)
	{
		if (Value >= LimitValue)
		{
			return;
		}
		Value += AddValue;
	}

	void FAOTFMath::ReduceWithLimit(float& Value, float LimitValue, float ReduceValue)
	{
		if (Value <= LimitValue)
		{
			return;
		}
		Value -= ReduceValue;
	}
}

