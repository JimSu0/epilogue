// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GameDefine.h"

namespace GameDefine
{
	const FName InputCharacteristic::COMBINATION = TEXT("Combination");
	const FName InputCharacteristic::BREAK = TEXT("Break");
	const FName InputCharacteristic::PASS = TEXT("PASS");
	//----------------------------------------------------------------------------------------------------
	const FName MontageName::NONE = TEXT("NONE");
	const FName MontageName::LIGHT_ATTACK1 = TEXT("LightAttack1");
	const FName MontageName::LIGHT_ATTACK2 = TEXT("LightAttack2");
	const FName MontageName::LIGHT_ATTACK3 = TEXT("LightAttack3");
	const FName MontageName::HEAVY_ATTACK = TEXT("HeavyAttack");
	const FName MontageName::DODGE = TEXT("Dodge");
	const FName MontageName::SLIDE = TEXT("Slide");
	
	// 被击中
	const FName MontageName::Attacked = TEXT("Attacked");
	// 攻击
	const FName MontageName::Attack = TEXT("Attack");
	//----------------------------------------------------------------------------------------------------
	const FName FChatSectionName::NONE = TEXT("NONE");
	const FName FChatSectionName::S000_ALEY = TEXT("S000_Aley");
}
