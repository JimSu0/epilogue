// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GameInstanceSubsystem/AnimationGameInstanceSubsystem.h"
#include "Game/GameDefine.h"
#include "GameFramework/Character.h"

void UAnimationGameInstanceSubsystem::PlayMontage(ACharacter* Character, FName MontageName)
{
	// 检验明蒙太奇动画名
	if (MontageName == GameDefine::MontageName::NONE)
	{
		return;
	}
	// 空值校验
	if (!IsValid(Character))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::PlayMontage : Character is nullptr"));
		return;
	}
	if (!MontageMap.Contains(MontageName))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::PlayMontage : Don't has this key [ %s ]"), *(MontageName.ToString()));
		return;
	}
	// 播放蒙太奇动画
	Character->PlayAnimMontage(MontageMap[MontageName]);
}

void UAnimationGameInstanceSubsystem::PlayMontageWithData(ACharacter* Character, FName MontageName, float InPlayRate, FName StartSectionName)
{
	// 检验明蒙太奇动画名
	if (MontageName == GameDefine::MontageName::NONE)
	{
		return;
	}
	// 空值校验
	if (!IsValid(Character))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::PlayMontage : Character is nullptr"));
		return;
	}
	if (!MontageMap.Contains(MontageName))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::PlayMontage : Don't has this key [ %s ]"), *(MontageName.ToString()));
		return;
	}
	// 播放蒙太奇动画
	Character->PlayAnimMontage(MontageMap[MontageName], InPlayRate, StartSectionName);
}

void UAnimationGameInstanceSubsystem::PlayMontage(ACharacter* Character, FName LastMontageName, FName CurrentMontageName)
{
	// 检验明蒙太奇动画名
	if (CurrentMontageName == GameDefine::MontageName::NONE)
	{
		return;
	}
	// 空值校验
	if (!IsValid(Character))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::StopMontage : Character is nullptr"));
		return;
	}
	if (!MontageMap.Contains(CurrentMontageName))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::StopMontage : Don't has this key [ %s ]"), *(CurrentMontageName.ToString()));
		return;
	}
	// 播放蒙太奇动画
	Character->StopAnimMontage(MontageMap[LastMontageName]);
	Character->PlayAnimMontage(MontageMap[CurrentMontageName]);
}

void UAnimationGameInstanceSubsystem::PlayMontage(ACharacter* Character, FName LastMontageName,
	FName CurrentMontageName, float InPlayRate, FName StartSectionName)
{
	// 检验明蒙太奇动画名
	if (CurrentMontageName == GameDefine::MontageName::NONE)
	{
		return;
	}
	// 空值校验
	if (!IsValid(Character))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::StopMontage : Character is nullptr"));
		return;
	}
	if (!MontageMap.Contains(CurrentMontageName))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::StopMontage : Don't has this key [ %s ]"), *(CurrentMontageName.ToString()));
		return;
	}
	// 播放蒙太奇动画
	Character->StopAnimMontage(MontageMap[LastMontageName]);
	Character->PlayAnimMontage(MontageMap[CurrentMontageName], InPlayRate, StartSectionName);
}

void UAnimationGameInstanceSubsystem::StopMontage(ACharacter* Character, FName MontageName)
{
	// 检验明蒙太奇动画名
	if (MontageName == GameDefine::MontageName::NONE)
	{
		return;
	}
	// 空值校验
	if (!IsValid(Character))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::StopMontage : Character is nullptr"));
		return;
	}
	if (!MontageMap.Contains(MontageName))
	{
		UE_LOG(LogTemp, Error, TEXT("UAnimationGameInstanceSubsystem::StopMontage : Don't has this key [ %s ]"), *(MontageName.ToString()));
		return;
	}
	// 播放蒙太奇动画
	Character->StopAnimMontage(MontageMap[MontageName]);
}

UAnimationGameInstanceSubsystem::UAnimationGameInstanceSubsystem()
{
	// 写入所有需要按路径加载的动画路径(后续可改为配表)
	static ConstructorHelpers::FObjectFinder<UAnimMontage> LightAttack1Montage(TEXT("AnimMontage'/Game/Animation/AM/AM_K_Attack/K_Attack_LB1_1.K_Attack_LB1_1'"));
	static ConstructorHelpers::FObjectFinder<UAnimMontage> LightAttack2Montage(TEXT("AnimMontage'/Game/Animation/AM/AM_K_Attack/K_Attack_LB1_2.K_Attack_LB1_2'"));
	static ConstructorHelpers::FObjectFinder<UAnimMontage> LightAttack3Montage(TEXT("AnimMontage'/Game/Animation/AM/AM_K_Attack/K_Attack_LB1_3.K_Attack_LB1_3'"));
	static ConstructorHelpers::FObjectFinder<UAnimMontage> HeavyAttackMontage(TEXT("AnimMontage'/Game/Animation/AM/AM_K_Attack/K_Attack_FocusEnergy_R1.K_Attack_FocusEnergy_R1'"));
	static ConstructorHelpers::FObjectFinder<UAnimMontage> DodgeMontage(TEXT("AnimMontage'/Game/Animation/AM/AM_K_Move/K_Dodge.K_Dodge'"));
	static ConstructorHelpers::FObjectFinder<UAnimMontage> SlideMontage(TEXT("AnimMontage'/Game/Animation/AM/AM_K_Move/K_Slide.K_Slide'"));

	// New Montage
	static ConstructorHelpers::FObjectFinder<UAnimMontage> AttackedMontage(TEXT("AnimMontage'/Game/Animation/AM/New/K_Attacked.K_Attacked'"));
	static ConstructorHelpers::FObjectFinder<UAnimMontage> AttackMontage(TEXT("AnimMontage'/Game/Animation/AM/New/K_Attack.K_Attack'"));
	
	// 将动作资源添加到映射中
	MontageMap.Add(GameDefine::MontageName::LIGHT_ATTACK1, LightAttack1Montage.Object);
	MontageMap.Add(GameDefine::MontageName::LIGHT_ATTACK2, LightAttack2Montage.Object);
	MontageMap.Add(GameDefine::MontageName::LIGHT_ATTACK3, LightAttack3Montage.Object);
	MontageMap.Add(GameDefine::MontageName::HEAVY_ATTACK, HeavyAttackMontage.Object);
	MontageMap.Add(GameDefine::MontageName::DODGE, DodgeMontage.Object);
	MontageMap.Add(GameDefine::MontageName::SLIDE, SlideMontage.Object);

	// New Montage
	MontageMap.Add(GameDefine::MontageName::Attacked, AttackedMontage.Object);
	MontageMap.Add(GameDefine::MontageName::Attack, AttackMontage.Object);
}

void UAnimationGameInstanceSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
}