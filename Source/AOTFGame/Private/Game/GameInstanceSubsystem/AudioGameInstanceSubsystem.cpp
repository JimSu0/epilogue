// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GameInstanceSubsystem/AudioGameInstanceSubsystem.h"

#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

void UAudioGameInstanceSubsystem::PlayAudioEffect(EAudioEffect SoundEffect)
{
	if (!SoundEffectMap.Contains(SoundEffect))
	{
		return;
	}
	if (SoundEffectPlayer == nullptr)
	{
		// 播放器为nullptr时生成一个播放器
		SoundEffectPlayer = UGameplayStatics::CreateSound2D(GetWorld(), SoundEffectMap[SoundEffect]);
	}
	// 设置音效
	SoundEffectPlayer->SetSound(SoundEffectMap[SoundEffect]);
	// 播放音效
	SoundEffectPlayer->Play();
	// 设置音量
}

void UAudioGameInstanceSubsystem::PlayAudioBGM(EAudioBGM BGM)
{
	if (!SoundBGMMap.Contains(BGM))
	{
		return;
	}
	CurrentBGM = BGM;
	if (SoundBGMPlayer == nullptr)
	{
		// 播放器为nullptr时生成一个播放器
		SoundBGMPlayer = UGameplayStatics::CreateSound2D(GetWorld(), SoundBGMMap[CurrentBGM]);
	}
	// 设置BGM
	SoundBGMPlayer->SetSound(SoundBGMMap[CurrentBGM]);
	// 播放BGM
	SoundBGMPlayer->Play();
	// 设置音量
	//SoundBGMPlayer->SetVolumeMultiplier(0.5f);
	
}

void UAudioGameInstanceSubsystem::PauseBGM(bool bPause)
{
	if (CurrentBGM == EAudioBGM::NONE)
	{
		UE_LOG(LogTemp, Warning, TEXT("UAudioGameInstanceSubsystem::PauseBGM : Current BGM is none"));
		return;
	}
	if (SoundBGMPlayer == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UAudioGameInstanceSubsystem::PauseBGM : SoundBGMPlayer is nullptr"))
		return;
	}
	// 暂停
	SoundBGMPlayer->SetPaused(bPause);
}

void UAudioGameInstanceSubsystem::AudioKnobSelectUp()
{
	// TODO::先穷举手写
	
	// 已选中到最顶端
	if (CurrentKnob == EAudioKnob::MASTER)
	{
		return;
	}
	else if (CurrentKnob == EAudioKnob::MUSIC)
	{
		CurrentKnob = EAudioKnob::MASTER;
	}
	else if (CurrentKnob == EAudioKnob::EFFECT)
	{
		CurrentKnob = EAudioKnob::MUSIC;
	}
}

void UAudioGameInstanceSubsystem::AudioKnobSelectDown()
{
	// TODO::先穷举手写
	
	// 已选中到最底端
	if (CurrentKnob == EAudioKnob::EFFECT)
	{
		return;
	}
	else if (CurrentKnob == EAudioKnob::MUSIC)
	{
		CurrentKnob = EAudioKnob::EFFECT;
	}
	else if (CurrentKnob == EAudioKnob::MASTER)
	{
		CurrentKnob = EAudioKnob::MUSIC;
	}
}

void UAudioGameInstanceSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	// 按照路径加载音频资源
	// 音效
	USoundWave* Jump1 = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Jump1.Jump1'"));
	USoundWave* Jump2 = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Jump2.Jump2'"));
	USoundWave* Slide1 = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Pick.Pick'"));
	USoundWave* Slide2 = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Slide1.Slide1'"));
	USoundWave* Pick = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Slide2.Slide2'"));
	USoundWave* Attack1 = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Attack1.Attack1'"));
	USoundWave* Attack2 = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Attack2.Attack2'"));
	USoundWave* Hit = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Hit.Hit'"));
	USoundWave* Collision = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Collision.Collision'"));
	USoundWave* Falling = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Falling.Falling'"));
	USoundWave* Select = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Select.Select'"));
	USoundWave* Press = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/SoundEffect/Press.Press'"));
	
	// 添加音效
	SoundEffectMap.Add(EAudioEffect::JUMP1, Jump1);
	SoundEffectMap.Add(EAudioEffect::JUMP2, Jump2);
	SoundEffectMap.Add(EAudioEffect::SLIDE1, Slide1);
	SoundEffectMap.Add(EAudioEffect::SLIDE2, Slide2);
	SoundEffectMap.Add(EAudioEffect::PICK, Pick);
	SoundEffectMap.Add(EAudioEffect::ATTACK1, Attack1);
	SoundEffectMap.Add(EAudioEffect::ATTACK2, Attack2);
	SoundEffectMap.Add(EAudioEffect::HIT, Hit);
	SoundEffectMap.Add(EAudioEffect::COLLISION, Collision);
	SoundEffectMap.Add(EAudioEffect::FALLING, Falling);
	SoundEffectMap.Add(EAudioEffect::SELECT, Select);
	SoundEffectMap.Add(EAudioEffect::PRESS, Press);

	// BGM
	USoundWave* BGMARID = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/BGM/ARID.ARID'"));
	USoundWave* BGMINTRO = LoadObject<USoundWave>(this, TEXT("SoundWave'/Game/Audio/BGM/Intro.Intro'"));
	// 添加BGM
	SoundBGMMap.Add(EAudioBGM::ARID, BGMARID);
	SoundBGMMap.Add(EAudioBGM::INTRO, BGMINTRO);
}
