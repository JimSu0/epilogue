// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/GameInstanceSubsystem/DialogueGameInstanceSubsystem.h"

#include "Player/DialogueComponent/DialogueActorComponent.h"

void UDialogueGameInstanceSubsystem::StartDialogue(UDialogueActorComponent* ActorComponent, UDialogueActorComponent* AnotherActorComponent)
{
	ActorDialogueComponent = ActorComponent;
	AnotherActorDialogueComponent = AnotherActorComponent;
	ActorDialogueComponent->SetIdentificationStr(1);
	AnotherActorDialogueComponent->SetIdentificationStr(2);
}

void UDialogueGameInstanceSubsystem::EndDialogue()
{
	// 还原数据
	ActorDialogueComponent->SetIdentificationStr(0);
	ActorDialogueComponent->SetIdentificationStr(0);
	
	ActorDialogueComponent = nullptr;
	AnotherActorDialogueComponent = nullptr;
}

void UDialogueGameInstanceSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
}
