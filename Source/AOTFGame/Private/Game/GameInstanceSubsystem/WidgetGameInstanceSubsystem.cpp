// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/GameInstanceSubsystem/WidgetGameInstanceSubsystem.h"

#include "Player/Widget/AOTFUserWidget.h"
#include "Player/Widget/Start/AOTFDialogueUserWidget.h"
#include "Player/Widget/Start/AOTFRadioUserWidget.h"

void UWidgetGameInstanceSubsystem::ShowWidget(EWidgetType WidgetType)
{
	if (WidgetType == EWidgetType::NONE)
	{
		return;
	}
	if (!WidgetObjectMap.Contains(WidgetType))
	{
		UE_LOG(LogTemp, Warning, TEXT("Don't has this type widget obget"));
		return;
	}
	// 判断是否否正在显示
	if (IsShow(WidgetType))
	{
		return;
	}
	// 根据类型创建一个Widget对象存放到列表中
	auto Widget = CreateWidget<UAOTFUserWidget>(GetWorld(), WidgetObjectMap[WidgetType]);
	OnShowWidgetList.Add(Widget);
	// 执行显示蓝图逻辑
	Widget->Show(Widget);
}

void UWidgetGameInstanceSubsystem::HideWidget(EWidgetType WidgetType)
{
	if (!WidgetObjectMap.Contains(WidgetType))
	{
		return;
	}
	// 去显示列表中找到对应类型的界面并隐藏
	for (int i = 0; i < OnShowWidgetList.Num(); ++i)
	{
		auto Widget = OnShowWidgetList[i];
		if (Widget->GetWidgetType() == WidgetType)
		{
			// 从显示列表中移除
			OnShowWidgetList.Remove(Widget);
			// 执行隐藏蓝图逻辑
			Widget->Hide(Widget);
			return;
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("未找到该类型的Widget, 请查看Widget蓝图的WidgetType属性是否配置正确"));
}

bool UWidgetGameInstanceSubsystem::IsShow(EWidgetType WidgetType)
{
	return Contains(OnShowWidgetList, WidgetType);
}

UAOTFUserWidget* UWidgetGameInstanceSubsystem::GetWidget(EWidgetType WidgetType)
{
	UAOTFUserWidget* Widget = nullptr;
	// 在显示界面表中查询
	return Find(OnShowWidgetList, WidgetType);
}

UAOTFUserWidget* UWidgetGameInstanceSubsystem::GetTopWidget()
{
	if (OnShowWidgetList.Num() <= 0)
	{
		return nullptr;
	}
	return OnShowWidgetList.Last();
}

bool UWidgetGameInstanceSubsystem::Contains(TArray<UAOTFUserWidget*> WidgetList, EWidgetType WidgetType)
{
	if (WidgetList.Num() == 0)
	{
		return false;
	}
	// 判断是否否正在显示
	for (auto Widget : WidgetList)
	{
		if (Widget->GetWidgetType() == WidgetType)
		{
			return true;
		}
	}
	return false;
}

UAOTFUserWidget* UWidgetGameInstanceSubsystem::Find(TArray<UAOTFUserWidget*> WidgetList, EWidgetType WidgetType)
{
	if (WidgetList.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("WidgetList is Empty"));
		return false;
	}
	// 判断是否否正在显示
	for (auto Widget : WidgetList)
	{
		if (Widget->GetWidgetType() == WidgetType)
		{
			return Widget;
		}
	}
	return nullptr;	
}


void UWidgetGameInstanceSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	WidgetObjectMap.Add(EWidgetType::LOGO, LoadClass<UAOTFUserWidget>(this, TEXT("WidgetBlueprint'/Game/Blueprint/Widget/BP_Logo.BP_Logo_C'")));
	WidgetObjectMap.Add(EWidgetType::RADIO, LoadClass<UAOTFRadioUserWidget>(this, TEXT("WidgetBlueprint'/Game/Blueprint/Widget/BP_Radio.BP_Radio_C'")));
	WidgetObjectMap.Add(EWidgetType::DIALOGUE, LoadClass<UAOTFDialogueUserWidget>(this, TEXT("WidgetBlueprint'/Game/Blueprint/Widget/BP_Dialogue.BP_Dialogue_C'")));
}
