// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/AOTFCharacter.h"

#include "Game/GameInstanceSubsystem/AnimationGameInstanceSubsystem.h"
#include "Player/InteractiveComponent/InteractByRayActorComponent.h"
#include "Game/GameInstanceSubsystem/WidgetGameInstanceSubsystem.h"
#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "Game/GameInstanceSubsystem/AudioGameInstanceSubsystem.h"
#include "Player/DialogueComponent/DialogueActorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Game/GameState/AOTFBattleGameState.h"
#include "GameFramework/SpringArmComponent.h"
#include "Player/AOTFPlayerController.h"
#include "Player/NPC/AOTFNPCCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Player/AOTFPlayerState.h"
#include "Camera/CameraComponent.h"
#include "Chaos/Core.h"
#include "Game/GameDefine.h"

void AAOTFCharacter::LaunchYZ(FVector2D LaunchVectorScaleYZ, float inAirScale)
{
	// 角色朝向
	auto Forward = GetActorForwardVector();
	// 推进力
	FVector LaunchVelocity;
	// 根据不同情况做区分
	if (GetCharacterMovement()->IsFalling())
	{
		// 在空中没有摩擦力，所以将推进力给小一些
		LaunchVelocity = FVector(0, Forward.Y * LaunchVectorScaleYZ.X, LaunchVectorScaleYZ.Y) * inAirScale;
	}
	else
	{
		// 在地上有摩擦力，所以将推进力给大一些
		LaunchVelocity = FVector(0, Forward.Y * LaunchVectorScaleYZ.X, LaunchVectorScaleYZ.Y);
	}
	// 推进Character
	LaunchCharacter(LaunchVelocity, false, false);
}

void AAOTFCharacter::Launch2D(FVector LaunchForce, float LaunchCoefficient)
{
	// 推进Character
	LaunchCharacter(FVector(0, LaunchForce.Y, LaunchForce.Z) * LaunchCoefficient, false, false);
}

void AAOTFCharacter::IntoGamePlayingMode()
{
	// 移动摄像头到屏幕中心位置
	CameraBoom->SocketOffset = FVector(0.0f,350.0f,75.0f);
	// 进入游戏状态
	AOTFPlayerState->SetInGamePlayingMode(true);
	// 播放游戏BGM
	AOTFPlayerController->GetAudioSubsystem()->PlayAudioBGM(EAudioBGM::ARID);
	// 隐藏Logo界面
	AOTFPlayerController->GetWidgetSubsystem()->HideWidget(EWidgetType::LOGO);
}

UAnimationGameInstanceSubsystem* AAOTFCharacter::GetAnimationSubsystem() const
{
	return AnimationSubsystem;
}

AAOTFPlayerController* AAOTFCharacter::GetAOTFPlayerController() const
{
	return AOTFPlayerController;
}

AAOTFPlayerState* AAOTFCharacter::GetAOTFPlayerState() const
{
	return AOTFPlayerState;
}

TArray<FDialogueLine> AAOTFCharacter::GetDialogueLineDataArr()
{
	switch (DialogueLineArrIndex)
	{
	case 1:
		return DialogueLineArr1;
	case 2:
		return DialogueLineArr2;
	case 3:
		return DialogueLineArr3;
	default:
		UE_LOG(LogTemp, Error, TEXT("AAOTFCharacter::GetDialogueLineData : Empty List"));
		TArray<FDialogueLine> Arr = {};
		return Arr;
	}
}

AAOTFCharacter::AAOTFCharacter()
{
	// 构造移动所需的相关组件
	// 初始化碰撞组件
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	// 控制器旋转时角色不旋转
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	// 创建弹簧臂并设置相关数据
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));
	// 创建相机并设置相关数据
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera
	// 设置移动组件数据
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, -720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;
	// 输入缓冲区
	InputBuffer = CreateDefaultSubobject<UInputBufferActorComponent>(TEXT("InputBuffer"));
	// 交互组件
	InteractComponent = CreateDefaultSubobject<UInteractByRayActorComponent>(TEXT("InteractiveComponent"));
	// 对话组件
	PlayerDialogueComponent = CreateDefaultSubobject<UDialogueActorComponent>(TEXT("DialogueComponent"));
}

void AAOTFCharacter::Interact(UInteractByRayActorComponent* Component)
{
	RayInteractComponent = Component;
}

void AAOTFCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f,-1.f,0.f), Value);
}

void AAOTFCharacter::StartJump()
{
	// InputBuffer->Input(EInputType::JUMP);
	Jump();
}

void AAOTFCharacter::Slide()
{
	InputBuffer->Input(EInputType::SLIDE);
}

void AAOTFCharacter::Dodge()
{
	InputBuffer->Input(EInputType::DODGE);
}

void AAOTFCharacter::LightAttackStart()
{
	// InputBuffer->Input(EInputType::LIGHT_ATTACK);
	AOTFPlayerState->bAttack = true;
	AnimationSubsystem->PlayMontage(this, GameDefine::MontageName::Attack);
}

void AAOTFCharacter::LightAttackEnd()
{
	AOTFPlayerState->bAttack = false;
}

void AAOTFCharacter::HeavyAttack()
{
	InputBuffer->Input(EInputType::HEAVY_ATTACK);
}

void AAOTFCharacter::Hook()
{
	InputBuffer->Input(EInputType::HOOK);
}

void AAOTFCharacter::PauseGame()
{
	AOTFPlayerController->GetAudioSubsystem()->PauseBGM(AOTFPlayerController->GetAOTFBattleGameState()->GetIsGamePause());
}

void AAOTFCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	// 在此处获取所有子系统
	// 获取AnimationGameInstanceSubsystem
	AnimationSubsystem = GetGameInstance()->GetSubsystem<UAnimationGameInstanceSubsystem>();
	if (AnimationSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFCharacter::BeginPlay : AnimationSubsystem is null ptr"));
	}
	
	// 获取PlayerState
	AOTFPlayerState = GetPlayerState<AAOTFPlayerState>();
	if (AOTFPlayerState == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFCharacter::BeginPlay : AOTFPlayerState is null ptr"));
		return;
	}
	// 获取PlayerController
	AOTFPlayerController = Cast<AAOTFPlayerController>(AOTFPlayerState->GetOwner());
	if (AOTFPlayerState == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFCharacter::BeginPlay : AOTFPlayerController is null ptr"));
		return;
	}
	
	// 在此处初始化所有组件
	// 初始化输入组件
	InputBuffer->Init(this, AOTFPlayerState);
	// 初始化交互组件
	InteractComponent->Init(this, FVector(-500.0f, 0, 0), [&](UInteractByRayActorComponent* Component)
	{
		Interact(Component);
	});
	// 初始化对话组件
	PlayerDialogueComponent->Init(this);

	// 设玩家X轴位置
	CharacterXOffset = GetActorLocation().X;
}

void AAOTFCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	FVector Location = GetActorLocation();
	SetActorLocation(FVector(CharacterXOffset, Location.Y, Location.Z));
}
