// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/AOTFPlayerController.h"

#include "Game/GameInstanceSubsystem/DialogueGameInstanceSubsystem.h"
#include "Player/InteractiveComponent/InteractByRayActorComponent.h"
#include "Game/GameInstanceSubsystem/WidgetGameInstanceSubsystem.h"
#include "Game/GameInstanceSubsystem/SceneActorInstanceSubsystem.h"
#include "Game/GameInstanceSubsystem/AudioGameInstanceSubsystem.h"
#include "Player/DialogueComponent/DialogueActorComponent.h"
#include "Game/GameState/AOTFBattleGameState.h"
#include "Player/Widget/AOTFUserWidget.h"
#include "Player/AOTFPlayerState.h"
#include "Player/AOTFCharacter.h"

UWidgetGameInstanceSubsystem* AAOTFPlayerController::GetWidgetSubsystem() const
{
	return WidgetSubsystem;
}

UAudioGameInstanceSubsystem* AAOTFPlayerController::GetAudioSubsystem() const
{
	return AudioSubsystem;
}

UDialogueGameInstanceSubsystem* AAOTFPlayerController::GetDialogueSubsystem() const
{
	return DialogueSubsystem;
}

AAOTFCharacter* AAOTFPlayerController::GetAOTFCharactor() const
{
	return AOTFCharacter;
}

AAOTFPlayerState* AAOTFPlayerController::GetAOTFPlayerState() const
{
	return AOTFPlayerState;
}

AAOTFBattleGameState* AAOTFPlayerController::GetAOTFBattleGameState() const
{
	return AOTFBattleGameState;
}

void AAOTFPlayerController::ShowWidget(EWidgetType WidgetType)
{
	if (WidgetSubsystem->IsShow(WidgetType))
	{
		return;
	}
	WidgetSubsystem->ShowWidget(WidgetType);
}

void AAOTFPlayerController::HideWidget(EWidgetType WidgetType)
{
	if (!WidgetSubsystem->IsShow(WidgetType))
	{
		return;
	}
	WidgetSubsystem->HideWidget(WidgetType);
}

void AAOTFPlayerController::MoveRight(float Value)
{
	if (AOTFPlayerState == nullptr || AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (AOTFPlayerState->IsCanMove() && AOTFPlayerState->IsCanControllerMove())
	{
		AOTFCharacter->MoveRight(Value);
	}
}

void AAOTFPlayerController::Interact()
{
	// 优先处理射线检测
	auto RayInteractComponent = AOTFCharacter->GetRayInteractiveComponent();
	if (RayInteractComponent == nullptr)
	{
		return;
	}
	// 如果当前正在交互
	if (AOTFPlayerState->IsInteracted())
	{
		//ExitInteract();
		// // 退出交互
		// RayInteractComponent->ExitInteract(this);
		// // 结束对话
		// // DialogueSubsystem->EndDialogue();
		// // 关闭对话界面
		// WidgetSubsystem->HideWidget(EWidgetType::DIALOGUE);
		// // 设置移动状态
		// AOTFPlayerState->SetCanMove(true);
	}
	else
	{
		EnterInteract();
		// RayInteractComponent->InterInteract(this);
		// // 获取对话组件
		// auto DialogueComponent = Cast<UDialogueActorComponent>(RayInteractComponent->GetOwnerActor()->GetComponentByClass(UDialogueActorComponent::StaticClass()));
		// // 判断当前交互对象是否需要对话
		// if (DialogueComponent != nullptr)
		// {
		// 	// 开始对话, 设置对话数据
		// 	DialogueSubsystem->StartDialogue(AOTFCharacter->GetDialogueComponent(), DialogueComponent);
		// 	// 打开对话界面
		// 	WidgetSubsystem->ShowWidget(EWidgetType::DIALOGUE);
		// }

		// // 打开对话界面
		// WidgetSubsystem->ShowWidget(EWidgetType::DIALOGUE);
		// 设置移动状态
		// AOTFPlayerState->SetCanMove(false);
	}
	// // 设置当前交互状态
	// AOTFPlayerState->SetInteracted(!AOTFPlayerState->IsInteracted());
}

void AAOTFPlayerController::Jump()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	if (AOTFPlayerState->IsAir())
	{
		return;
	}
	// 随机播放两个跳跃音效
	int8 bRandomNum = FMath::RandBool();
	if (bRandomNum)
	{
		// 播放音效
		AudioSubsystem->PlayAudioEffect(EAudioEffect::JUMP1);	
	}
	else
	{
		AudioSubsystem->PlayAudioEffect(EAudioEffect::JUMP2);
	}
	// 播放特效等操作
	OnJumpDo();
	AOTFCharacter->StartJump();
}

void AAOTFPlayerController::StopJumping()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	AOTFCharacter->StopJumping();
}

void AAOTFPlayerController::Slide()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	AOTFCharacter->Slide();
}

void AAOTFPlayerController::Dodge()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	AOTFCharacter->Dodge();
}

void AAOTFPlayerController::LightAttackStart()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	// 播放攻击音效
	AudioSubsystem->PlayAudioEffect(EAudioEffect::ATTACK1);
	AOTFCharacter->LightAttackStart();
}

void AAOTFPlayerController::LightAttackEnd()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	AOTFCharacter->LightAttackEnd();
}

void AAOTFPlayerController::HeavyAttack()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	AOTFCharacter->HeavyAttack();
}

void AAOTFPlayerController::Hook()
{
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode() && !AOTFPlayerState->IsCanMove())
	{
		return;
	}
	// 正在交互时不使用钩索
	if (AOTFPlayerState->IsInteracted())
	{
		return;
	}
	AOTFCharacter->Hook();
}

void AAOTFPlayerController::PauseGame()
{
	if (AOTFBattleGameState == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Null ptr"))
		return;
	}
	if (!AOTFPlayerState->IsInGamePlayingMode())
	{
		return;
	}
	AOTFBattleGameState->SetGamePause(!AOTFBattleGameState->GetIsGamePause());
	AOTFCharacter->PauseGame();
}

void AAOTFPlayerController::ScrollerControl(float Value)
{
	// Value为音量改变速率
	
	// 获取当前界面
	auto Widget = WidgetSubsystem->GetTopWidget();
	// 根据界面判断是在控制什么
	// 因为现在仅支持单页面显示, 所以不做当前选中界面输入互相影响处理(后续有需求可重写)
	if (Widget == nullptr)
	{
		return;
	}
	Widget->OnScrollerDo(Value);
	// switch (Widget->GetWidgetType())
	// {
	// // 当前为音量界面
	// case EWidgetType::RADIO:
	// 	auto RadioWidget = Cast<UAOTFRadioUserWidget>(Widget);
	// 	if (RadioWidget == nullptr)
	// 	{
	// 		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::ScrollerControl : UAOTFRadioUserWidget type exchange failed"));
	// 		break;
	// 	}
	// 	// 操作数据
	// 	// 获取当前操作的音量按键
	// 	auto AudioKnob = AudioSubsystem->GetAudioKnob();
	// 	// 根据当前按键设置音量
	// 	if (AudioKnob == EAudioKnob::MASTER)
	// 	{
	// 		AOTFPlayerState->TurnMasterVolume(Value);
	// 	}
	// 	else if (AudioKnob == EAudioKnob::MUSIC)
	// 	{
	// 		AOTFPlayerState->TurnMusicVolume(Value);
	// 	}
	// 	else if (AudioKnob == EAudioKnob::EFFECT)
	// 	{
	// 		AOTFPlayerState->TurnEffectVolume(Value);
	// 	}
	// 	// 刷新界面
	// 	RadioWidget->RefreshVolume();
	// 	break;
	// }
}

void AAOTFPlayerController::SelectUp()
{
	// 获取当前界面
	auto Widget = WidgetSubsystem->GetTopWidget();
	// 根据界面判断是在控制什么
	// 因为现在仅支持单页面显示, 所以不做当前选中界面输入互相影响处理(后续有需求可重写)
	if (Widget == nullptr)
	{
		return;
	}
	Widget->OnSelectDo(true);
	// switch (Widget->GetWidgetType())
	// {
	// 	// 当前为音量界面
	// 	case EWidgetType::RADIO:
	// 		auto RadioWidget = Cast<UAOTFRadioUserWidget>(Widget);
	// 		if (RadioWidget == nullptr)
	// 		{
	// 			UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::ScrollerControl : UAOTFRadioUserWidget type exchange failed"));
	// 			break;
	// 		}
	// 		// 操作数据
	// 		AudioSubsystem->AudioKnobSelectUp();
	// 		// 刷新界面
	// 		RadioWidget->RefreshKnobSelected();
	// 	break;
	// }
}

void AAOTFPlayerController::SelectDown()
{
	// 获取当前界面
	auto Widget = WidgetSubsystem->GetTopWidget();
	// 根据界面判断是在控制什么
	// 因为现在仅支持单页面显示, 所以不做当前选中界面输入互相影响处理(后续有需求可重写)
	if (Widget == nullptr)
	{
		return;
	}
	Widget->OnSelectDo(false);
	// switch (Widget->GetWidgetType())
	// {
	// 	// 当前为音量界面
	// 	case EWidgetType::RADIO:
	// 		auto RadioWidget = Cast<UAOTFRadioUserWidget>(Widget);
	// 	if (RadioWidget == nullptr)
	// 	{
	// 		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::ScrollerControl : UAOTFRadioUserWidget type exchange failed"));
	// 		break;
	// 	}
	// 	// 操作数据
	// 	AudioSubsystem->AudioKnobSelectDown();
	// 	// 刷新界面
	// 	RadioWidget->RefreshKnobSelected();
	// 	break;
	// }
}

void AAOTFPlayerController::GoNext()
{
	// 获取当前界面
	auto Widget = WidgetSubsystem->GetTopWidget();
	// 根据界面判断是在控制什么
	// 因为现在仅支持单页面显示, 所以不做当前选中界面输入互相影响处理(后续有需求可重写)
	if (Widget == nullptr)
	{
		return;
	}
	Widget->OnGoNextDo();
}

void AAOTFPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	// 获取Subsystem
	// 获取Widget控制子系统
	WidgetSubsystem = GetGameInstance()->GetSubsystem<UWidgetGameInstanceSubsystem>();
	if (WidgetSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::BeginPlay : WidgetSubsystem is null ptr"));
	}
	// 获取Animation控制子系统
	AudioSubsystem = GetGameInstance()->GetSubsystem<UAudioGameInstanceSubsystem>();
	if (AudioSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFCharacter::BeginPlay : AudioSubsystem is null ptr"));
	}
	// 初始化对话子系统
	DialogueSubsystem = GetGameInstance()->GetSubsystem<UDialogueGameInstanceSubsystem>();
	if (DialogueSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFNPCCharacter::BeginPlay : DialogueGameInstanceSubsystem is null ptr"));
	}
	SceneActorSubsystem = GetGameInstance()->GetSubsystem<USceneActorInstanceSubsystem>();
	if (SceneActorSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFNPCCharacter::BeginPlay : SceneActorGameInstanceSubsystem is null ptr"));
	}

	
	// Controller下子对象获取
	// 获取Character
	AOTFCharacter = Cast<AAOTFCharacter>(GetCharacter());
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::BeginPlay : AOTFCharacter is null ptr"));
	}
	// 获取PlayerState
	AOTFPlayerState = Cast<AAOTFPlayerState>(PlayerState);
	if (AOTFPlayerState == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::BeginPlay : AOTFPlayerState is null ptr"));
	}

	// Game相关子对象获取
	// 获取GameState
	auto GameState = GetWorld()->GetGameState();
	if (GameState != nullptr)
	{
		// 获取当前GameState
		AOTFBattleGameState = Cast<AAOTFBattleGameState>(GameState);
	}
	if (AOTFBattleGameState == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerController::BeginPlay : AOTFBattleGameState is null ptr"));
	}

	
	// 游戏开始, 显示LOGO界面
	if (WidgetSubsystem != nullptr)
	{
		WidgetSubsystem->ShowWidget(EWidgetType::LOGO);
	}
	// 游戏开始, 播放BGM
	if (AudioSubsystem != nullptr)
	{
		AudioSubsystem->PlayAudioBGM(EAudioBGM::INTRO);
	}
}

void AAOTFPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	// 绑定输入逻辑
	InputComponent->BindAction("Interact", IE_Pressed, this, &AAOTFPlayerController::Interact);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AAOTFPlayerController::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &AAOTFPlayerController::StopJumping);
	
	InputComponent->BindAction("NormalAttack", IE_Pressed, this, &AAOTFPlayerController::LightAttackStart);
	InputComponent->BindAction("NormalAttack", IE_Released, this, &AAOTFPlayerController::LightAttackEnd);
	
	InputComponent->BindAction("HeavyAttack", IE_Pressed, this, &AAOTFPlayerController::HeavyAttack);
	InputComponent->BindAction("Hook", IE_Pressed, this, &AAOTFPlayerController::Hook);
	InputComponent->BindAction("Slide", IE_Pressed, this, &AAOTFPlayerController::Slide);
	InputComponent->BindAction("Dodge", IE_Pressed, this, &AAOTFPlayerController::Dodge);
	InputComponent->BindAction("Pause", IE_Pressed, this, &AAOTFPlayerController::PauseGame);
	InputComponent->BindAxis("MoveRight", this, &AAOTFPlayerController::MoveRight);

	// Widget Input
	InputComponent->BindAxis("ScrollerControl", this, &AAOTFPlayerController::ScrollerControl);
	InputComponent->BindAction("SelectUp", IE_Pressed, this, &AAOTFPlayerController::SelectUp);
	InputComponent->BindAction("SelectDown", IE_Pressed, this, &AAOTFPlayerController::SelectDown);
	InputComponent->BindAction("Next", IE_Pressed, this, &AAOTFPlayerController::GoNext);
}

void AAOTFPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (AOTFPlayerState == nullptr)
	{
		return;
	}
	if (AOTFPlayerState->IsInGamePlayingMode())
	{
		if (!AOTFBattleGameState->GetIsGamePause())
		{
			// 游戏模式非暂停情况下向右一直移动
			AOTFCharacter->MoveRight(1.0f);
		}
		else
		{
			// 游戏模式暂停情况下还原移动
			AOTFCharacter->MoveRight(0.0f);
		}
	}
}

void AAOTFPlayerController::EnterInteract()
{
	auto RayInteractComponent = AOTFCharacter->GetRayInteractiveComponent();
	if (RayInteractComponent == nullptr)
	{
		return;
	}
	RayInteractComponent->InterInteract(this);
	// 获取对话组件
	auto DialogueComponent = Cast<UDialogueActorComponent>(RayInteractComponent->GetOwnerActor()->GetComponentByClass(UDialogueActorComponent::StaticClass()));
	// 判断当前交互对象是否需要对话
	if (DialogueComponent != nullptr)
	{
		// 开始对话, 设置对话数据
		DialogueSubsystem->StartDialogue(AOTFCharacter->GetDialogueComponent(), DialogueComponent);
		// 打开对话界面
		WidgetSubsystem->ShowWidget(EWidgetType::DIALOGUE);
	}
	// 打开对话界面
	WidgetSubsystem->ShowWidget(EWidgetType::DIALOGUE);
	// 设置移动状态
	AOTFPlayerState->SetCanMove(false);
	// 设置当前交互状态
	AOTFPlayerState->SetInteracted(!AOTFPlayerState->IsInteracted());
}

void AAOTFPlayerController::ExitInteract()
{
	auto RayInteractComponent = AOTFCharacter->GetRayInteractiveComponent();
	if (RayInteractComponent == nullptr)
	{
		return;
	}
	// 退出交互
	RayInteractComponent->ExitInteract(this);
	// 关闭对话界面
	WidgetSubsystem->HideWidget(EWidgetType::DIALOGUE);
	// 设置移动状态
	AOTFPlayerState->SetCanMove(true);
	// 设置当前交互状态
	AOTFPlayerState->SetInteracted(!AOTFPlayerState->IsInteracted());
}
