// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/AOTFPlayerState.h"

#include "AOTFUtils/AOTFUtils.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Player/AOTFCharacter.h"

void AAOTFPlayerState::TurnMasterVolume(float Scale)
{
	AOTFUtils::FAOTFMath::CalcWithLimit(CurrentMasterVolume, MinMasterVolume, MaxMasterVolume, 0.1f * Scale);
}

void AAOTFPlayerState::TurnMusicVolume(float Scale)
{
	AOTFUtils::FAOTFMath::CalcWithLimit(CurrentMusicVolume, MinMusicVolume, MaxMusicVolume, 0.1f * Scale);
}

void AAOTFPlayerState::TurnEffectVolume(float Scale)
{
	AOTFUtils::FAOTFMath::CalcWithLimit(CurrentEffectVolume, MinEffectVolume, MaxEffectVolume, 0.1f * Scale);
}

bool AAOTFPlayerState::IsAir() const
{
	if (AOTFCharacter != nullptr)
	{
		return AOTFCharacter->GetMovementComponent()->IsFalling();
	}
	UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerState::IsJump : AOTFCharacter is null ptr"));
	return false;
}

void AAOTFPlayerState::SetInGamePlayingMode(bool Is)
{
	bInGamePlayingMode = Is;
	bCanControllerMove = !Is;
}

void AAOTFPlayerState::BeginPlay()
{
	Super::BeginPlay();
	AOTFCharacter = Cast<AAOTFCharacter>(GetPawn());
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AAOTFPlayerState::BeginPlay : AOTFCharacter is null ptr"));
	}
}
