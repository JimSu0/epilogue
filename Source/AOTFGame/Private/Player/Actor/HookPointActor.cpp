// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Actor/HookPointActor.h"

#include "Player/InteractiveComponent/InteractByKnockActorComponent.h"
#include "Player/AOTFCharacter.h"

AHookPointActor::AHookPointActor()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	InteractComponent = CreateDefaultSubobject<UInteractByKnockActorComponent>("InteractiveComponent");
	auto Trigger = (USceneComponent*)InteractComponent->GetSphereTrigger();
	RootComponent = Trigger;
	Mesh->SetupAttachment(Trigger);
}

void AHookPointActor::Interact(AActor* InteractActor)
{
	if (InteractActor == nullptr)
	{
		AOTFCharacter->SetHookPointActor(nullptr);
		return;
	}
	// 是玩家自身碰撞上, 将玩家的钩爪点设置为自身
	if (Cast<AAOTFCharacter>(InteractActor) == AOTFCharacter)
	{
		UE_LOG(LogTemp, Warning, TEXT("Can catch : %s"), *GetName());
		AOTFCharacter->SetHookPointActor(this);
	}
}

void AHookPointActor::BeginPlay()
{
	Super::BeginPlay();
	auto Controller = GetGameInstance()->GetFirstLocalPlayerController();
	if (Controller != nullptr)
	{
		AOTFCharacter = Cast<AAOTFCharacter>(Controller->GetPawn());
	}
	if (AOTFCharacter == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("AHookPointActor::BeginPlay : AOTFCharacter is null ptr"));
	}
	InteractComponent->Init(this, [&](AActor* InteractiveActor)
	{
		Interact(InteractiveActor);
	});
}

