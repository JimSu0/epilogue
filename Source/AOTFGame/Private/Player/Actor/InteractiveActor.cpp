// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Actor/InteractiveActor.h"

#include "Components/BoxComponent.h"
#include "Player/DialogueComponent/DialogueActorComponent.h"
#include "Player/InteractiveComponent/InteractByRayActorComponent.h"

UInteractByRayActorComponent* AInteractiveActor::GetInteractComponent() const
{
	return InteractComponent;
}

UDialogueActorComponent* AInteractiveActor::GetDialogueComponent() const
{
	return DialogueComponent;
}

TArray<FDialogueLine> AInteractiveActor::GetDialogueLineDataArr()
{
	switch (DialogueLineArrIndex)
	{
	case 1:
		return DialogueLineArr1;
	case 2:
		return DialogueLineArr2;
	case 3:
		return DialogueLineArr3;
	default:
		UE_LOG(LogTemp, Error, TEXT("AAOTFCharacter::GetDialogueLineData : Empty List"));
		TArray<FDialogueLine> Arr = {};
		return Arr;
	}
}

AInteractiveActor::AInteractiveActor()
{
	BoxCollider = CreateDefaultSubobject<UBoxComponent>("BoxCollider");
	SetRootComponent(BoxCollider);

	ActorMesh = CreateDefaultSubobject<UStaticMeshComponent>("ActorMesh");	
	ActorMesh->SetupAttachment(BoxCollider);
	
	InteractComponent = CreateDefaultSubobject<UInteractByRayActorComponent>("InteractiveComponent");
	DialogueComponent = CreateDefaultSubobject<UDialogueActorComponent>("DialogueComponent");
}

void AInteractiveActor::BeginPlay()
{
	Super::BeginPlay();
	InteractComponent->Init(this, RayEndPosition, [&](UInteractByRayActorComponent* Component)
	                        {
		                        Interact(Component);
	                        }, [&](AAOTFPlayerController* PlayerController)
	                        {
		                        InterInteract(PlayerController);
	                        }, [&](AAOTFPlayerController* PlayerController)
	                        {
		                        ExitInteract(PlayerController);
	                        });
	// 初始化对话组件
	DialogueComponent->Init(this);
}

