// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Actor/ModeChangeActor.h"

#include "Components/BoxComponent.h"
#include "Player/AOTFCharacter.h"

// Sets default values
AModeChangeActor::AModeChangeActor()
{
	BoxCollider = CreateDefaultSubobject<UBoxComponent>("BoxCollider");
	SetRootComponent(BoxCollider);
	// 绑定碰撞事件
	BoxCollider->OnComponentBeginOverlap.AddUniqueDynamic(this, &AModeChangeActor::OverlapBegins);
}

void AModeChangeActor::OverlapBegins(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto AOTFCharacter = Cast<AAOTFCharacter>(OtherActor);
	if (AOTFCharacter == nullptr)
	{
		return;
	}
	// 放置多次触发
	if (!bChanged)
	{
		// 进入游戏模式
		AOTFCharacter->IntoGamePlayingMode();
		bChanged = true;
	}
}

// Called when the game starts or when spawned
void AModeChangeActor::BeginPlay()
{
	Super::BeginPlay();

	
}

