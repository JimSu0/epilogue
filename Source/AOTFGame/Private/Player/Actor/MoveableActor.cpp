// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Actor/MoveableActor.h"

#include "Components/BoxComponent.h"
#include "Game/GameInstanceSubsystem/SceneActorInstanceSubsystem.h"
#include "Player/NPC/AOTFNPCCharacter.h"

AMoveableActor::AMoveableActor()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxCollider");
	SetRootComponent(BoxComponent);
	ActorMesh = CreateDefaultSubobject<UStaticMeshComponent>("MoveableActorMesh");
	ActorMesh->SetupAttachment(BoxComponent);
}

// Called when the game starts or when spawned
void AMoveableActor::BeginPlay()
{
	Super::BeginPlay();
	// if (NPCCharacter == nullptr)
	// {
	// 	UE_LOG(LogTemp, Error, TEXT("AMoveableActor::BeginPlay : NPCCharacter is nullptr, please set NPCCharacter in blueprint first!!!"));
	// 	return;
	// }
	// NPCCharacter->DialogueEnd.AddLambda([=]()
	// {
	// 	this->Move(ActorMesh);
	// });
}

void AMoveableActor::Register(FString ActorName)
{
	GetGameInstance()->GetSubsystem<USceneActorInstanceSubsystem>()->SceneActorMap.Add(ActorName, this);
}

