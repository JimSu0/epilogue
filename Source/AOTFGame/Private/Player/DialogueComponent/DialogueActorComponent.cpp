// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/DialogueComponent/DialogueActorComponent.h"

#include "Player/AOTFCharacter.h"
#include "Player/Actor/InteractiveActor.h"
#include "Player/NPC/AOTFNPCCharacter.h"

void UDialogueActorComponent::Init(AActor* Owner)
{
	OwnerActor = Owner;
}

TArray<FDialogueLine> UDialogueActorComponent::GetDialogueLineArr()
{
	TArray<FDialogueLine> Arr;
	if (OwnerActor == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UDialogueActorComponent::GetDialogueLineArr : OwnerActor is nullptr"));
		return Arr;
	}
	auto Character = Cast<AAOTFCharacter>(OwnerActor);
	if (Character != nullptr)
	{
		return Character->GetDialogueLineDataArr();
	}
	
	auto NPCCharacter = Cast<AAOTFNPCCharacter>(OwnerActor);
	if (NPCCharacter != nullptr)
	{
		return NPCCharacter->GetDialogueLineDataArr();
	}
	
	auto InteractiveActor = Cast<AInteractiveActor>(OwnerActor);
	if (InteractiveActor != nullptr)
	{
		return InteractiveActor->GetDialogueLineDataArr();
	}
	
	UE_LOG(LogTemp, Error, TEXT("UDialogueActorComponent::GetDialogueLineArr : Empty List"));
	return Arr;
}

// Sets default values for this component's properties
UDialogueActorComponent::UDialogueActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDialogueActorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

