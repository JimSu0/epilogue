// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputBufferActorComponent.h"

#include "Player/InputBufferComponent/InputKey/LightAttackKey.h"
#include "Player/InputBufferComponent/InputKey/InputKey.h"
#include "Player/InputBufferComponent/InputKey/DodgeKey.h"
#include "Player/InputBufferComponent/InputKey/SlideKey.h"
#include "Player/InputBufferComponent/InputKey/JumpKey.h"
#include "Player/AOTFPlayerState.h"
#include "Player/AOTFCharacter.h"
#include "Game/GameDefine.h"
#include "Player/InputBufferComponent/InputKey/HookKey.h"

void UInputBufferActorComponent::Init(AAOTFCharacter* Character, AAOTFPlayerState* PlayerState)
{
	AOTFCharacter = Character;
	AOTFPlayerState = PlayerState;
}

void UInputBufferActorComponent::Input(EInputType KeyType)
{
	if (KeyType == EInputType::EMPTY)
	{
		return;
	}
	// 生成按键对象
	auto InputKey = GenerateInputKey(KeyType);
	if (InputKey == nullptr)
	{
		return;
	}
	// 获取这个对象的类型
	auto Type = InputKey->GetInputType();
	if (KeyStateMap.Contains(Type))
	{
		// 如果当前按键状态为已按, 不加入按键列表
		if (KeyStateMap[Type])
		{
			StoredKeyList.Add(InputKey);
		}
		// 传入表中当前按键对应的状态
		InputKey->Input(KeyStateMap[Type]);
	}
	else
	{
		// 新建当前按键状态键值对
		KeyStateMap.Add(Type, true);
		InputKey->Input(KeyStateMap[Type]);
		StoredKeyList.Add(InputKey);
	}
	// 根据当前按键特性处理连击列表
	// 按键特性为BREAK的按键会打断所有的按键
	if (InputKey->GetCharacteristic() == GameDefine::InputCharacteristic::BREAK)
	{
		// 清空连击按键列表
		StoredLightAttackKeyList.Empty();
		// 清空连招按键列表
		StoredComboKeyList.Empty();
	}
}

AAOTFCharacter* UInputBufferActorComponent::GetAOTFCharacter() const
{
	return AOTFCharacter;
}

AAOTFPlayerState* UInputBufferActorComponent::GetAOTFPlayerState() const
{
	return AOTFPlayerState;
}

// Sets default values for this component's properties
UInputBufferActorComponent::UInputBufferActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

FInputKey* UInputBufferActorComponent::GenerateInputKey(EInputType Type)
{
	switch (Type)
	{
	case EInputType::JUMP:
		return (FInputKey*)new FJumpKey(this);
	case EInputType::SLIDE:
		return (FInputKey*)new FSlideKey(this);
	case EInputType::DODGE:
		return (FInputKey*)new FDodgeKey(this);
	case EInputType::MOVE_UP:
		return nullptr;
	case EInputType::MOVE_DOWN:
		return nullptr;
	case EInputType::LIGHT_ATTACK:
		return (FInputKey*)new FLightAttackKey(this);
	case EInputType::HEAVY_ATTACK:
		return nullptr;
	case EInputType::HOOK:
		return (FInputKey*)new FHookKey(this);
	default:
		return nullptr;
	}
}

void UInputBufferActorComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                               FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	// 输入列表不为空时, 运行子元素的冷却计时
	if (StoredKeyList.Num() != 0)
	{
		for (int i = 0; i < StoredKeyList.Num(); ++i)
		{
			auto Key = StoredKeyList[i];
			// 冷却计时, 冷却是否结束
			bool bChillDownOver = Key->ChillDownTick(KeyStateMap[StoredKeyList[i]->GetInputType()]);
			do
			{	
				// 打断操作判断
				if (i >= StoredKeyList.Num() - 1)
				{
					// 当前按键为最后一位时忽略
					break;
				}
				if (Key->GetCharacteristic() != GameDefine::InputCharacteristic::COMBINATION)
				{
					// 当前按键特性不为COMBINATION，则冷却计时不会被打断
					break;
				}
				auto NextKey = StoredKeyList[i + 1];
				if (NextKey->GetCharacteristic() == GameDefine::InputCharacteristic::BREAK)
				{
					// 下一按键特性为BREAK，则当前按键会被打断
					Key->ChillDownEnd(KeyStateMap[Key->GetInputType()]);
					bChillDownOver = true;
					break;
				}
			}
			while (false);
			
			if (!bChillDownOver)
			{
				// 冷却未结束
				continue;
			}
			// 冷却结束后推出按键列表
			StoredKeyList.RemoveAt(i);
			// 释放按键
			//free(Key);
			if (i > 0)
			{
				// 索引回退
				--i;
			}
		}
	}
}
