// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputKey/DodgeKey.h"

#include "Game/GameInstanceSubsystem/AnimationGameInstanceSubsystem.h"
#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "GameFramework/Character.h"
#include "Player/AOTFCharacter.h"
#include "Game/GameDefine.h"

FDodgeKey::FDodgeKey(UInputBufferActorComponent* InputBufferActorComponent) : FInputKey(InputBufferActorComponent)
{
	InputType = EInputType::DODGE;
	Characteristic = GameDefine::InputCharacteristic::BREAK;
	MontageName = GameDefine::MontageName::DODGE;
	ChillDownFrame = 84;

	// 设置角色推进力数值
	DodgeLaunchVelocityYMultiple = 100;
	LaunchVelocityZ = 100.0f;
}

FDodgeKey::~FDodgeKey()
{
	FInputKey::~FInputKey();
}

void FDodgeKey::OnInputDo(bool& bCanInput)
{
	FInputKey::OnInputDo(bCanInput);
	if (bCanInput == false)
	{
		// 冷却中
		return;
	}
	auto Character = InputBuffer->GetAOTFCharacter(); 
	// 推进Character
	Character->LaunchYZ(FVector2D(DodgeLaunchVelocityYMultiple, LaunchVelocityZ), 0.6f);
	// 播放蒙太奇动画
	Character->GetAnimationSubsystem()->PlayMontage(Character, MontageName);
	// TODO : 播放特效
	UE_LOG(LogTemp, Warning, TEXT("Dodge input"))
}

void FDodgeKey::OnChillDownDo()
{
	FInputKey::OnChillDownDo();
	UE_LOG(LogTemp, Warning, TEXT("Dodge colling is over"))
}
