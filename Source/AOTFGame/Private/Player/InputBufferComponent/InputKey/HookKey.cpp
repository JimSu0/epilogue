// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputKey/HookKey.h"

#include "Game/GameInstanceSubsystem/AnimationGameInstanceSubsystem.h"
#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "GameFramework/Character.h"
#include "Player/AOTFCharacter.h"
#include "Game/GameDefine.h"

FHookKey::FHookKey(UInputBufferActorComponent* InputBufferActorComponent) : FInputKey(InputBufferActorComponent)
{
	InputType = EInputType::HOOK;
	Characteristic = GameDefine::InputCharacteristic::BREAK;
	MontageName = GameDefine::MontageName::NONE;
	ChillDownFrame = 10;

	// 设置角色推进力数值
	LaunchCoefficient = 2000;
}

FHookKey::~FHookKey()
{
	FInputKey::~FInputKey();
}

void FHookKey::OnInputDo(bool& bCanInput)
{
	FInputKey::OnInputDo(bCanInput);
	auto Character = InputBuffer->GetAOTFCharacter();
	auto CatchPoint = Character->GetHookPointActor(); 
	// 冷却中或者没有钩索抓点
	if (bCanInput == false || CatchPoint == nullptr)
	{
		// 冷却中
		return;
	}
	// 获取从玩家位置指向钩锁抓点方向的标准向量
	auto ForceVector = CatchPoint->GetActorLocation() - Character->GetActorLocation();
	ForceVector.Normalize();
	// 推进Character
	Character->Launch2D(ForceVector, LaunchCoefficient);
	// 播放蒙太奇动画
	Character->GetAnimationSubsystem()->PlayMontage(Character, MontageName);
	// TODO : 播放特效
	UE_LOG(LogTemp, Warning, TEXT("Hook input"))
}

void FHookKey::OnChillDownDo()
{
	FInputKey::OnChillDownDo();
	UE_LOG(LogTemp, Warning, TEXT("Hook colling is over"))
}
