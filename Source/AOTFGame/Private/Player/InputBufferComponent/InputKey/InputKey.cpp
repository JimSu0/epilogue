// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputKey/InputKey.h"

#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "Game/GameDefine.h"

void FInputKey::Input(bool& bCanInput)
{
	OnInputDo(bCanInput);
	bCanInput = false;
}

bool FInputKey::ChillDownTick(bool& bCanInput)
{
	// 先禁用冷却帧LOG
	// UE_LOG(LogTemp, Warning, TEXT("CurrentFrame : %d"), CurrentFrame);
	
	if (CurrentFrame == SpecifiedFrame)
	{
		// 当运行到指定帧时执行
		OnSpecifiedFrameDo();
	}
	if (CurrentFrame >= ChillDownFrame)
	{
		// 优先允许输入
		bCanInput = true;
		OnChillDownDo();
		CurrentFrame = 0;
		return true;
	}
	++CurrentFrame;
	return false;
}

void FInputKey::ChillDownEnd(bool& bCanInput)
{
	bCanInput = true;
	OnChillDownDo();
	CurrentFrame = 0;
}

FInputKey::FInputKey(UInputBufferActorComponent* InputBufferActorComponent)
{
	InputType = EInputType::EMPTY;
	Characteristic = GameDefine::InputCharacteristic::PASS;
	MontageName = GameDefine::MontageName::NONE;
	ChillDownFrame = 0;
	CurrentFrame = 0;
	// 指定执行帧默认为冷却结束帧
	SpecifiedFrame = ChillDownFrame;
	InputBuffer = InputBufferActorComponent;
	// // 根据输入类型判断对应的类型与蒙太奇动画名
	// switch (InputType)
	// {
	// case EInputType::HEAVY_ATTACK:
	// 	Characteristic = GameDefine::InputCharacteristic::COMBINATION;
	// 	MontageName = GameDefine::MontageName::HEAVY_ATTACK;
	// 	ChillDownFrame = 260;
	// 	break;
	// case EInputType::AIR_ATTACK:
	// 	Characteristic = GameDefine::InputCharacteristic::COMBINATION;
	// 	// 蒙太奇动画暂时置为NONE
	// 	MontageName = GameDefine::MontageName::NONE;
	// 	break;
	// case EInputType::MOVE_DOWN:
	// 	Characteristic = GameDefine::InputCharacteristic::COMBINATION;
	// 	MontageName = GameDefine::MontageName::NONE;
	// 	break;
	// case EInputType::MOVE_UP:
	// 	Characteristic = GameDefine::InputCharacteristic::COMBINATION;
	// 	MontageName = GameDefine::MontageName::NONE;
	// 	break;
	// case EInputType::TOGGLE_WEAPON:
	// 	Characteristic = GameDefine::InputCharacteristic::BREAK;
	// 	// 蒙太奇动画暂时置为NONE
	// 	MontageName = GameDefine::MontageName::NONE;
	// 	break;
	// case EInputType::EMPTY:
	// 	Characteristic = GameDefine::InputCharacteristic::PASS;
	// 	MontageName = GameDefine::MontageName::NONE;
	// 	ChillDownFrame = 0;
	// 	break;
	// }
}
