// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputKey/JumpKey.h"

#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "Player/AOTFPlayerState.h"
#include "Player/AOTFCharacter.h"
#include "Game/GameDefine.h"
#include "Game/GameInstanceSubsystem/AudioGameInstanceSubsystem.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Player/AOTFPlayerController.h"


FJumpKey::FJumpKey(UInputBufferActorComponent* InputBufferActorComponent) : FInputKey(InputBufferActorComponent)
{
	InputType = EInputType::JUMP;
	Characteristic = GameDefine::InputCharacteristic::BREAK;
	MontageName = GameDefine::MontageName::NONE;
	ChillDownFrame = 120;
}

FJumpKey::~FJumpKey()
{
	FInputKey::~FInputKey();
	UE_LOG(LogTemp, Warning, TEXT("Delete JumpKey"));
}

void FJumpKey::OnInputDo(bool& bCanInput)
{
	FInputKey::OnInputDo(bCanInput);
	auto PlayerState = InputBuffer->GetAOTFPlayerState();
	// 已经按下跳跃键或者在空中时再次按下跳跃按键按键会执行二段跳
	if (bCanInput == false || PlayerState->IsAir())
	{
		if (!PlayerState->IsTwoStageJump())
		{
			// 执行二段跳(需要二段跳蒙太奇动画)
			// 设置二段跳状态
			PlayerState->SetTwoStageJump(true);
			// 播放二段跳蒙太奇动画
			
			// 给玩家一个向上的推进力
			InputBuffer->GetAOTFCharacter()->LaunchYZ(FVector2D(100, 400));
		}
		// 冷却中
		return;
	}
	InputBuffer->GetAOTFCharacter()->Jump();
	UE_LOG(LogTemp, Warning, TEXT("Jump input"));
	// 随机播放两个跳跃音效
	int8 bRandomNum = FMath::RandBool();
	if (bRandomNum)
	{
		// 播放音效
		InputBuffer->GetAOTFCharacter()->GetAOTFPlayerController()->GetAudioSubsystem()->PlayAudioEffect(EAudioEffect::JUMP1);	
	}
	else
	{
		InputBuffer->GetAOTFCharacter()->GetAOTFPlayerController()->GetAudioSubsystem()->PlayAudioEffect(EAudioEffect::JUMP2);
	}
}

void FJumpKey::OnChillDownDo()
{
	FInputKey::OnChillDownDo();
	InputBuffer->GetAOTFCharacter()->GetAOTFPlayerState()->SetTwoStageJump(false);
	UE_LOG(LogTemp, Warning, TEXT("Jump colling is over"));
}