// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputKey/LightAttackKey.h"

#include "Game/GameInstanceSubsystem/AnimationGameInstanceSubsystem.h"
#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "Player/AOTFPlayerState.h"
#include "Player/AOTFCharacter.h"
#include "Game/GameDefine.h"

FLightAttackKey::FLightAttackKey(UInputBufferActorComponent* InputBufferActorComponent) : FInputKey(InputBufferActorComponent)
{
	LastComboKey = nullptr;
	LightAttackEndCallBack = nullptr;
	HasNextLihtAttack = false;
	auto ComboNum = InputBuffer->GetLightAttackKeyListNum();
	if (ComboNum >= InputBufferActorComponent->GetLightAttackMaxCount())
	{
		InputType = EMPTY;
		MontageName = GameDefine::MontageName::NONE;
		Characteristic = GameDefine::InputCharacteristic::PASS;
		ChillDownFrame = 0;
		SpecifiedFrame = 0;
		return;
	}
	InputType = LIGHT_ATTACK;
	Characteristic = GameDefine::InputCharacteristic::COMBINATION;
	// 根据当前连击数判断需要播放的蒙太奇动画
	switch (ComboNum)
	{
	case 0:
		MontageName = GameDefine::MontageName::LIGHT_ATTACK1;
		ChillDownFrame = 136;
		SpecifiedFrame = 90;
		break;
	case 1:
		// TODO::判断上一个按键类型来决定要播放的蒙太奇动画
		MontageName = GameDefine::MontageName::LIGHT_ATTACK2;
		ChillDownFrame = 142;
		SpecifiedFrame = 90;
		break;
	case 2:
		MontageName = GameDefine::MontageName::LIGHT_ATTACK3;
		ChillDownFrame = 300;
		SpecifiedFrame = 154;
		break;
	default:
		InputType = EMPTY;
		MontageName = GameDefine::MontageName::NONE;
		Characteristic = GameDefine::InputCharacteristic::PASS;
		ChillDownFrame = 0;
		SpecifiedFrame = 0;
		break;
	}
}

FLightAttackKey::~FLightAttackKey()
{
	FInputKey::~FInputKey();
}

bool FLightAttackKey::CanInput()
{
	return InputBuffer->GetLightAttackKeyListNum() < InputBuffer->GetLightAttackMaxCount();
}

TKeyValuePair<bool, int32> FLightAttackKey::GetIndex()
{
	// 获取自己在连击案件列表中的位置
	auto index = InputBuffer->GetStoredLightAttackKeyList().IndexOfByKey((FInputKey*)this);
	return TKeyValuePair<bool, int>(index >= 0, index);
}

void FLightAttackKey::OnInputDo(bool& bCanInput)
{
	FInputKey::OnInputDo(bCanInput);
	// 最多仅允许同同时输入三个轻击按键
	if (!CanInput())
	{
		return;
	}
	// 禁用玩家移动
	InputBuffer->GetAOTFPlayerState()->SetCanMove(false);
	// 添加自身进入连击列表
	InputBuffer->AddLightAttackList(this);

	// 获取自己在连击案件列表中的位置
	auto index = GetIndex().Value;
	if (index == -1)
	{
		UE_LOG(LogTemp, Error, TEXT("FLightAttackKey::OnInputDo : Con't find this key"));
		return;
	}
	if (index == 0)
	{
		// 如果自己是第一个连击按键, 直接播放
		InputBuffer->GetAOTFCharacter()->GetAnimationSubsystem()->PlayMontage(InputBuffer->GetAOTFCharacter(), MontageName);
	}
	else
	{
		// 设置上一个按键
		LastComboKey = InputBuffer->FindObjectInLightAttackKeyList(index - 1);
		// 如果上一个按键为轻击按键
		if (LastComboKey->GetInputType() == LIGHT_ATTACK)
		{
			// 将类型转换为轻击键
			auto LastLightKey = (FLightAttackKey*)LastComboKey;
			LastLightKey->HasNextLihtAttack = true;
			// 添加执行回调
			LastLightKey->LightAttackEndCallBack.BindLambda([&]
			{
				// 添加进按键冷却计时列表
				InputBuffer->AddKeyList(this);
				
				// 在上一个攻击动画播放完毕之后播放当前动画
				InputBuffer->GetAOTFCharacter()->GetAnimationSubsystem()->PlayMontage(InputBuffer->GetAOTFCharacter(), MontageName);			
			});
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Light attack input"))
}

void FLightAttackKey::OnChillDownDo()
{
	FInputKey::OnChillDownDo();
	auto index = GetIndex().Value;
	if (index == InputBuffer->GetLightAttackKeyListNum() - 1)
	{
		// 如果当前按键为最后一个按键, 清空连击按键列表
		InputBuffer->EmptyLightAttackKeyList();
		// 玩家可以移动
		InputBuffer->GetAOTFPlayerState()->SetCanMove(true);
	}
}

void FLightAttackKey::OnSpecifiedFrameDo()
{
	FInputKey::OnSpecifiedFrameDo();
	// 执行回调
	LightAttackEndCallBack.ExecuteIfBound();
}
