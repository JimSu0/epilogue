// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InputBufferComponent/InputKey/SlideKey.h"

#include "Game/GameInstanceSubsystem/AnimationGameInstanceSubsystem.h"
#include "Player/InputBufferComponent/InputBufferActorComponent.h"
#include "GameFramework/Character.h"
#include "Player/AOTFCharacter.h"
#include "Game/GameDefine.h"

FSlideKey::FSlideKey(UInputBufferActorComponent* InputBufferActorComponent) : FInputKey(InputBufferActorComponent)
{
	InputType = EInputType::SLIDE;
	Characteristic = GameDefine::InputCharacteristic::BREAK;
	MontageName = GameDefine::MontageName::SLIDE;
	ChillDownFrame = 90;
	
	// 设置角色推进力数值
	SlideLaunchVelocityYMultiple = 2000;
	LaunchVelocityZ = 100.0f;
}

FSlideKey::~FSlideKey()
{
	FInputKey::~FInputKey();
}

void FSlideKey::OnInputDo(bool& bCanInput)
{
	FInputKey::OnInputDo(bCanInput);
	if (bCanInput == false)
	{
		// 冷却中
		return;
	}
	auto Character = InputBuffer->GetAOTFCharacter();
	// 推进玩家
	Character->LaunchYZ(FVector2D(SlideLaunchVelocityYMultiple, LaunchVelocityZ), 0.6f);
	// 播放蒙太奇动画
	Character->GetAnimationSubsystem()->PlayMontage(Character, MontageName);
	// TODO : 播放特效
	UE_LOG(LogTemp, Warning, TEXT("Slide input"))
}

void FSlideKey::OnChillDownDo()
{
	FInputKey::OnChillDownDo();
	UE_LOG(LogTemp, Warning, TEXT("Slide colling is over"))
}
