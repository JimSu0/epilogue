// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/InteractiveComponent/InteractByKnockActorComponent.h"

#include "Components/SphereComponent.h"

void UInteractByKnockActorComponent::Init(AActor* Owner, TFunction<void(AActor*)> Callback)
{
	Owner = Owner;
	if (Callback != nullptr)
	{
		CollisionInteractCallback.BindLambda(Callback);
	}
}

// Sets default values for this component's properties
UInteractByKnockActorComponent::UInteractByKnockActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	// Create component
	SphereTrigger = CreateDefaultSubobject<USphereComponent>("SphereTrigger");
	SphereTrigger->OnComponentBeginOverlap.AddUniqueDynamic(this, &UInteractByKnockActorComponent::Interact);
	SphereTrigger->OnComponentEndOverlap.AddUniqueDynamic(this, &UInteractByKnockActorComponent::EndInteract);
	SphereTrigger->SetSphereRadius(600.0f);
}

void UInteractByKnockActorComponent::Interact(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CollisionInteractCallback.ExecuteIfBound(OtherActor);
}

void UInteractByKnockActorComponent::EndInteract(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                 UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	CollisionInteractCallback.ExecuteIfBound(nullptr);
}

// Called every frame
void UInteractByKnockActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

