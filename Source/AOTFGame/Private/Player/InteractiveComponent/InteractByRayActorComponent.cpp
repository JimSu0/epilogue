// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/InteractiveComponent/InteractByRayActorComponent.h"

#include "DrawDebugHelpers.h"

void UInteractByRayActorComponent::Init(AActor* Owner, FVector RayCastEndPosition,
                                        TFunction<void(UInteractByRayActorComponent*)> Component,
                                        TFunction<void(AAOTFPlayerController*)> InterCallback,
                                        TFunction<void(AAOTFPlayerController*)> ExitCallback)
{
	OwnerActor = Owner;
	RayEndPosition = RayCastEndPosition;
	if (Component != nullptr)
	{
		OnRayInteractive.AddLambda(Component);
	}
	if (InterCallback != nullptr)
	{
		OnRayInterInteractive.AddLambda(InterCallback);
	}
	if (ExitCallback != nullptr)
	{
		OnRayExitInteractive.AddLambda(ExitCallback);
	}
}

// Sets default values for this component's properties
UInteractByRayActorComponent::UInteractByRayActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UInteractByRayActorComponent::Interact(AActor* InteractActor)
{
	// 只有在交互对象改变时才检测
	if (InteractActor == CurrentInteractiveActor)
	{
		return;
	}
	// 设置当前交互对象
	CurrentInteractiveActor = InteractActor;
	// 当前交互对象为NULL
	if (CurrentInteractiveActor == nullptr)
	{
		// 传入空指针
		OnRayInteractive.Broadcast(nullptr);
		return;
	}
	// 获取交互组件
	auto Component = Cast<UInteractByRayActorComponent>(InteractActor->GetComponentByClass(UInteractByRayActorComponent::StaticClass()));
	if (Component == nullptr)
	{
		// 传入空指针
		OnRayInteractive.Broadcast(nullptr);
		return;
	}
	// 传入交互组件
	OnRayInteractive.Broadcast(Component);
}

void UInteractByRayActorComponent::InterInteract(AAOTFPlayerController* AOTFController)
{
	OnRayInterInteractive.Broadcast(AOTFController);
}

void UInteractByRayActorComponent::ExitInteract(AAOTFPlayerController* AOTFController)
{
	OnRayExitInteractive.Broadcast(AOTFController);
}


// Called when the game starts
void UInteractByRayActorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UInteractByRayActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (OwnerActor == nullptr)
	{
		return;
	}
	
	// 碰撞参数
	FCollisionQueryParams CollisonQueryParams(TEXT("QueryParams"), true, NULL);
	CollisonQueryParams.bTraceComplex = true;
	CollisonQueryParams.bReturnPhysicalMaterial = false;
	CollisonQueryParams.AddIgnoredActor(OwnerActor);
	
	// 射线检测
	auto LineStart = OwnerActor->GetActorLocation();
	// 单向检测
	auto LineEnd = LineStart + RayEndPosition;

	// 射线 Debug
	// DrawDebugLine(GetWorld(),LineStart, LineEnd, FColor::Red);
	
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult, LineStart, LineEnd, ECollisionChannel::ECC_Pawn, CollisonQueryParams);
	// 执行交互检测以及调用交互回调
	Interact(HitResult.GetActor());	
}