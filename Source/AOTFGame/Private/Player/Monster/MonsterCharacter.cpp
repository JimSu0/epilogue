// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Monster/MonsterCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Player/AOTFCharacter.h"
#include "Player/AOTFPlayerState.h"
#include "Player/InteractiveComponent/InteractByRayActorComponent.h"

// Sets default values
AMonsterCharacter::AMonsterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InteractComponent = CreateDefaultSubobject<UInteractByRayActorComponent>("InteractComponent");
}

// Called when the game starts or when spawned
void AMonsterCharacter::BeginPlay()
{
	Super::BeginPlay();
	// 初始化交互组件
	InteractComponent->Init(this, FVector(0.0f, DirectLength, 0.0f), [&](UInteractByRayActorComponent* Component)
	                        {
		                        Interact(Component);
	                        }, [&](AAOTFPlayerController* PlayerController)
	                        {
		                        InterInteract(PlayerController);
	                        }, [&](AAOTFPlayerController* PlayerController)
	                        {
		                        ExitInteract(PlayerController);
	                        });
}

// Called every frame
void AMonsterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMonsterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
