// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Widget/Start/AOTFDialogueUserWidget.h"
#include "Game/GameInstanceSubsystem/DialogueGameInstanceSubsystem.h"

UDialogueGameInstanceSubsystem* UAOTFDialogueUserWidget::GetDialogueSubsystem() const
{
	return GetGameInstance()->GetSubsystem<UDialogueGameInstanceSubsystem>();
}
