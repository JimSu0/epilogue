// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Widget/Start/AOTFRadioUserWidget.h"

#include "Game/GameInstanceSubsystem/AudioGameInstanceSubsystem.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UAOTFRadioUserWidget::RefreshImageKnob(UImage* MasterKnob, UTextBlock* MasterVolume, UImage* MusicKnob,
                                            UTextBlock* MusicVolume, UImage* EffectKnob, UTextBlock* EffectVolume)
{
	AudioSubsystem = GetGameInstance()->GetSubsystem<UAudioGameInstanceSubsystem>();
	if (AudioSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UAOTFRadioUserWidget::RefreshImageSelected : AudioSubsystem is nullptr"));
		return;
	}
	
}

void UAOTFRadioUserWidget::RefreshImageSelected(UImage* MasterKnobSelected, UImage* MusicKnobSelected, UImage* EffectKnobSelected)
{
	AudioSubsystem = GetGameInstance()->GetSubsystem<UAudioGameInstanceSubsystem>();
	if (AudioSubsystem == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UAOTFRadioUserWidget::RefreshImageSelected : AudioSubsystem is nullptr"));
		return;
	}
	if (AudioSubsystem->GetAudioKnob() == EAudioKnob::MASTER)
	{
		MasterKnobSelected->RenderOpacity = 1.0f;
		MusicKnobSelected->RenderOpacity = 0.0f;
		EffectKnobSelected->RenderOpacity = 0.0f;
	}
	else if (AudioSubsystem->GetAudioKnob() == EAudioKnob::MUSIC)
	{
		MasterKnobSelected->RenderOpacity = 0.0f;
		MusicKnobSelected->RenderOpacity = 1.0f;
		EffectKnobSelected->RenderOpacity = 0.0f;
	}
	else if (AudioSubsystem->GetAudioKnob() == EAudioKnob::EFFECT)
	{
		MasterKnobSelected->RenderOpacity = 0.0f;
		MusicKnobSelected->RenderOpacity = 0.0f;
		EffectKnobSelected->RenderOpacity = 1.0f;
	}
}
