// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Widget/Start/Dialogue/AOTFDialogueBoxUserWidget.h"

#include "Player/DialogueComponent/DialogueActorComponent.h"

void UAOTFDialogueBoxUserWidget::Init(UDialogueActorComponent* Component)
{
	DialogueComponent = Component;
	SetData(Component);
}

void UAOTFDialogueBoxUserWidget::FunctionInitData(FLineData Data)
{
	InitData(Data);
}

void UAOTFDialogueBoxUserWidget::FunctionInitRobotData(FRobotLineData Data)
{
	InitRobotData(Data);
}

FDialogueLine UAOTFDialogueBoxUserWidget::GetCurrentLineData()
{
	auto Arr = DialogueComponent->GetDialogueLineArr();
	FDialogueLine Data;
	if (Arr.Num() != 0)
	{
		Data = Arr[LineIndex];
	}
	return Data;
}
