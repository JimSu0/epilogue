// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Widget/Start/Dialogue/AOTFDialogueOptionUserWidget.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Game/GameInstanceSubsystem/DialogueGameInstanceSubsystem.h"

void UAOTFDialogueOptionUserWidget::Init(FDialogueLine Data, FName Line, bool CanSelected)
{
	LineData = Data;
	bCanSelected = CanSelected;
	LineStr = Line;
	SetData(LineData, LineStr, CanSelected);
}

void UAOTFDialogueOptionUserWidget::InitAditi(FLineData Data)
{
	SetAditiLineData(Data);
}

void UAOTFDialogueOptionUserWidget::InitRobot(FRobotLineData Data, int32 Index)
{
	SetRobotLineData(Data, Index);
}

void UAOTFDialogueOptionUserWidget::RefreshAditi(FLineData Data)
{
	SetAditiLineData(Data);
}

void UAOTFDialogueOptionUserWidget::RefreshRobot(FRobotLineData Data, int32 Index)
{
	SetRobotLineData(Data, Index);
}


void UAOTFDialogueOptionUserWidget::SetSelected(UImage* SelectImage, bool bIs)
{
	if (SelectImage == nullptr)
	{
		return;
	}
	if (!bIs)
	{
		SelectImage->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		SelectImage->SetVisibility(ESlateVisibility::Visible);
	}	
}

UDialogueGameInstanceSubsystem* UAOTFDialogueOptionUserWidget::GetDialogueSubsystem() const
{
	return GetGameInstance()->GetSubsystem<UDialogueGameInstanceSubsystem>();
}
