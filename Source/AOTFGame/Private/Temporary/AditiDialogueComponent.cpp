// Fill out your copyright notice in the Description page of Project Settings.


#include "Temporary/AditiDialogueComponent.h"

#include "Game/GameInstanceSubsystem/WidgetGameInstanceSubsystem.h"
#include "Player/AOTFPlayerController.h"
#include "Player/AOTFPlayerState.h"

void UAditiDialogueComponent::Init(AActor* Actor)
{
	OwnerActor = Actor;
	LineDataArr.Add(FLineData(Aditi, nullptr,TEXT("早安，集川先生。"), true));
	LineDataArr.Add(FLineData(Aditi, nullptr,TEXT("今天是列车启程的第两千零四十六天，车上的幸存人数依然只有您一人，距离下一站还有……"), true));
	LineDataArr.Add(FLineData(Player, nullptr,TEXT("不用汇报了，阿底提号，不会再有下一站了。"), false));
	LineDataArr.Add(FLineData(Aditi, nullptr,TEXT("那么，您已经决定要放弃寻找新城市了吗？"), true));
	LineDataArr.Add(FLineData(Player, nullptr,TEXT("我们这么多年来只是在一个又一个废墟间流窜罢了，人类的寿命可没那么多可以浪费的岁月。"), false));
	LineDataArr.Add(FLineData(Player, nullptr,TEXT("而且就算是作为AI，你也不想一生都在城市的残骸里穿行吧，没想过去看看这个世界上更美好的景色吗？"), false));
	LineDataArr.Add(FLineData(Aditi, nullptr,TEXT("当然！"), true));
	LineDataArr.Add(FLineData(Player, nullptr,TEXT("别急着高兴，要方向要到主控室才行。"), false));
	LineDataArr.Add(FLineData(Aditi, nullptr,TEXT("那就拜托您了……请结束这场没有意义的旅程吧。"), true));
}

FLineData UAditiDialogueComponent::DialogueStart()
{
	// 获取玩家状态
	auto PlayerState = Cast<AAOTFPlayerController>(GetWorld()->GetFirstPlayerController())->GetAOTFPlayerState();
	// 是否与Aditi交互过
	if (PlayerState->bAditi)
	{
		Index = LineDataArr.Num() - 1;
		return FLineData(Aditi, nullptr,TEXT("请一定要小心，我会在主控室等着您的。"), true);
	}
	return LineDataArr[Index];
}

FLineData UAditiDialogueComponent::GoNext()
{
	++Index;
	if (Index == LineDataArr.Num())
	{
		auto Controller = Cast<AAOTFPlayerController>(GetWorld()->GetFirstPlayerController());
		Controller->ExitInteract();
		Controller->GetAOTFPlayerState()->bAditi = true;
		Index = 0;
		return FLineData();
	}
	return LineDataArr[Index];
}