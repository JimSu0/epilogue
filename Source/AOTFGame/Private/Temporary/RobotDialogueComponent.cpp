// Fill out your copyright notice in the Description page of Project Settings.


#include "Temporary/RobotDialogueComponent.h"

#include "Game/GameInstanceSubsystem/SceneActorInstanceSubsystem.h"
#include "Game/GameInstanceSubsystem/WidgetGameInstanceSubsystem.h"
#include "Player/AOTFPlayerController.h"
#include "Player/AOTFPlayerState.h"
#include "Player/Actor/MoveableActor.h"
#include "Player/NPC/AOTFNPCCharacter.h"

void URobotDialogueComponent::Init(AActor* Actor)
{
	OwnerActor = Actor;
	// Arr1
	LineDataArr1.Add(FRobotLineData(Robot, nullptr,{TEXT("检测到乘客违规离开车厢，请返回安全区域。")}, true));
	LineDataArr1.Add(FRobotLineData(Player, nullptr,{TEXT("（还是无法通过吗……）")}, false));
	// Arr2
	LineDataArr2.Add(FRobotLineData(Robot, nullptr,{TEXT("检测到乘客违规离开车厢，请返回安全区域。")}, true));
	LineDataArr2.Add(FRobotLineData(Player, nullptr,{TEXT("！")}, false));
	LineDataArr2.Add(FRobotLineData(Aditi, nullptr,{TEXT("别紧张，我已经关闭了他的系统。")}, true));
	LineDataArr2.Add(FRobotLineData(Aditi, nullptr,{TEXT("但我没有权限干涉其他车厢的守卫，接下来的路可能很危险……")}, true));
	LineDataArr2.Add(FRobotLineData(Aditi, nullptr,{TEXT("你还是决定继续往前么?")}, true));
	// Arr3
	LineDataArr3.Add(FRobotLineData(Player, nullptr, {TEXT("【确定】"), TEXT("【取消】")}, false));
	// Arr4
	LineDataArr4.Add(FRobotLineData(Aditi, nullptr,{TEXT("做好准备了吗？")}, true));
	// Arr5
	LineDataArr5.Add(FRobotLineData(Aditi, nullptr,{TEXT("请一定要小心，我会在主控室等着您的。")}, true));
	// Arr6
	LineDataArr6.Add(FRobotLineData(Aditi, nullptr,{TEXT("没关系，做好准备再出发吧。")}, true));
}

FRobotLineData URobotDialogueComponent::DialogueStart()
{
	FRobotLineData Data;
	// 获取玩家状态
	auto PlayerState = Cast<AAOTFPlayerController>(GetWorld()->GetFirstPlayerController())->GetAOTFPlayerState();
	// 是否与Robot交互过
	if (PlayerState->bRobot)
	{
		CurrentArr = TEXT("Arr5");
		Data = LineDataArr5[Index];
	}
	else if (bDefultDialogue && bSelected)
	{
		// 常规选型走完且进行过选择操作
		// 常规对话进行完毕, 循环进行第四对话数组
		CurrentArr = TEXT("Arr4");
		Data = LineDataArr4[Index];
	}
	// 是否与Aditi交互过
	else if (PlayerState->bAditi)
	{
		CurrentArr = TEXT("Arr2");
		Data = LineDataArr2[Index];
	}
	else
	{
		CurrentArr = TEXT("Arr1");
		Data = LineDataArr1[Index];
	}
	return Data;
}

FRobotLineData URobotDialogueComponent::GoNext()
{
 	++Index;
	
	if (CurrentArr == TEXT("Arr1") && Index == LineDataArr1.Num())
	{
		return ResetLineData();
	}
	if (CurrentArr == TEXT("Arr2") && Index == LineDataArr2.Num())
	{
		// 常规对话已经结束
		bDefultDialogue = true;
		Index = 0;
		// 设置当前对话为Arr3
		CurrentArr = TEXT("Arr3");
	}
	if (CurrentArr == TEXT("Arr3") && Index == LineDataArr3.Num())
	{
		Index = 0;
		// 根据选择判断接下来的对话与操作
		if (CurrentSelect == TEXT("Yes"))
		{
			bSelected = false;
			// 设置当前对话为Arr5
			CurrentArr = TEXT("Arr5");
		}
		else if (CurrentSelect == TEXT("No"))
		{
			bSelected = true;
			// 设置当前对话为Arr6
			CurrentArr = TEXT("Arr6");
		}
		else
		{
			Index = 0;
			UE_LOG(LogTemp, Error, TEXT("Wrong current select str"));
		}
	}
	if (CurrentArr == TEXT("Arr4") && Index == LineDataArr4.Num())
	{
		Index = 0;
		// 提示选择
		CurrentArr = TEXT("Arr3");
	}
	if (CurrentArr == TEXT("Arr5") && Index == LineDataArr5.Num())
	{
		Index = 0;
		// 获取控制器
		auto Controller = Cast<AAOTFPlayerController>(GetWorld()->GetFirstPlayerController());
		// 设置与机器人交互状态
		Controller->GetAOTFPlayerState()->bRobot = true; 
		// 打开门(只执行一次)
		if (!bDoorOpen)
		{
			auto Actor1 = Cast<AMoveableActor>(Controller->SceneActorSubsystem->SceneActorMap["Door1"]);
			auto Actor2 = Cast<AMoveableActor>(Controller->SceneActorSubsystem->SceneActorMap["Door2"]);
			Actor1->Move(Actor1->ActorMesh);
			Actor2->Move(Actor2->ActorMesh);
			bDoorOpen = true;
		}
		// 退出交互
		Controller->ExitInteract();
		return FRobotLineData();
	}
	if (CurrentArr == TEXT("Arr6") && Index == LineDataArr6.Num())
	{
		Index = 0;
		// 返回开始界面
		return ResetLineData();
	}
	FRobotLineData Data;
	// 根据当前数组字符串选择返回的数据
	if (CurrentArr == TEXT("Arr1"))
	{
		Data = LineDataArr1[Index];
	}
	else if (CurrentArr == TEXT("Arr2"))
	{
		Data = LineDataArr2[Index];
	}
	else if (CurrentArr == TEXT("Arr3"))
	{
		Data = LineDataArr3[Index];
	}
	else if (CurrentArr == TEXT("Arr4"))
	{
		Data = LineDataArr4[Index];
	}
	else if (CurrentArr == TEXT("Arr5"))
	{
		Data = LineDataArr5[Index];
	}
	else if (CurrentArr == TEXT("Arr6"))
	{
		Data = LineDataArr6[Index];
	}
	else
	{
		return FRobotLineData();
	}
	return Data;
}

FRobotLineData URobotDialogueComponent::ResetLineData()
{
	auto Controller = Cast<AAOTFPlayerController>(GetWorld()->GetFirstPlayerController());
	Controller->ExitInteract();
	Index = 0;
	return FRobotLineData();
}
