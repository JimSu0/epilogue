// Fill out your copyright notice in the Description page of Project Settings.


#include "Test/StreamingLoadActor.h"

#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"



// Sets default values
AStreamingLoadActor::AStreamingLoadActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	OverlapVolum = CreateDefaultSubobject<UBoxComponent>("OverlapVolum");
    RootComponent = OverlapVolum;

    OverlapVolum->SetBoxExtent(FVector(232.0f, 8.0f, 144.0f));
	// 绑定碰撞事件
    OverlapVolum->OnComponentBeginOverlap.AddUniqueDynamic(this, &AStreamingLoadActor::OverlapBegins);
}

// Called when the game starts or when spawned
void AStreamingLoadActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStreamingLoadActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStreamingLoadActor::OverlapBegins(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACharacter* Character = UGameplayStatics::GetPlayerCharacter(this, 0);
	// 碰撞物为玩家并且加载关卡名称不为null
	if (OtherActor == Character && LevelToLoad != "")
	{
		if (LevelToLoad != TEXT(""))
		{
			FLatentActionInfo LatentInfoLoad;
			// 加载场景
			UGameplayStatics::LoadStreamLevel(this, LevelToLoad, true, true, LatentInfoLoad);
		}
		if (LevelToUnLoad != TEXT(""))
		{
			FLatentActionInfo LatentInfoUnLoad;
			// 卸载场景
			UGameplayStatics::UnloadStreamLevel(this, LevelToUnLoad, LatentInfoUnLoad, true);	
		}
	}
}

