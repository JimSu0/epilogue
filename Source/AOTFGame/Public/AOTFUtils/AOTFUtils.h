// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

namespace AOTFUtils
{
	class AOTFGAME_API FAOTFMath
	{
	public:
		// Calculation and modify Value with limit
		UFUNCTION(BlueprintCallable, Category="CPP_Function")
		static void CalcWithLimit(float& Value, float MinLimitValue, float MaxLimitValue, float DtValue);
		UFUNCTION(BlueprintCallable, Category="CPP_Function")
		static void AddWithLimit(float& Value, float LimitValue, float AddValue);
		UFUNCTION(BlueprintCallable, Category="CPP_Function")
		static void ReduceWithLimit(float& Value, float LimitValue, float ReduceValue);
	};	
}