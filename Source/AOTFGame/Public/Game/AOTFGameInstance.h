// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "AOTFGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAOTFGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
#pragma region Normal Fuction
public:
	UAOTFGameInstance();
#pragma endregion
};
