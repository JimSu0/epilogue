// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Game define property
 */
namespace GameDefine
{
 struct InputCharacteristic
 {
  // The type of input key characteristic
  static const FName COMBINATION;
  static const FName BREAK;
  static const FName PASS;
 };

 struct MontageName
 {
  // Montage name
  static const FName NONE;
  static const FName LIGHT_ATTACK1;
  static const FName LIGHT_ATTACK2;
  static const FName LIGHT_ATTACK3;
  static const FName HEAVY_ATTACK;
  static const FName DODGE;
  static const FName SLIDE;
  static const FName Attacked;
  static const FName Attack;
 };

 struct FChatSectionName
 {
  // Chat section name
  static const FName NONE;
  static const FName S000_ALEY;
 };
};
