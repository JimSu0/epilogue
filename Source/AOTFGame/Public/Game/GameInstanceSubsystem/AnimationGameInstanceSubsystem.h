// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "AnimationGameInstanceSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAnimationGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
// Static Const Property
#pragma region Static Const Property	
public:

#pragma endregion

//----------------------------------------------------------------------------------------------------	
// Fuction
#pragma region Interface Function
public:
	// Play the montage
	UFUNCTION(BlueprintCallable,Category="CPP_Function")
	void PlayMontage(ACharacter* Character, FName MontageName);
	// Play the montage with scale and startSectionName
	UFUNCTION(BlueprintCallable,Category="CPP_Function")
	void PlayMontageWithData(ACharacter* Character, FName MontageName, float InPlayRate, FName StartSectionName);
	// Stop last montage and play current montage
	void PlayMontage(ACharacter* Character, FName LastMontageName, FName CurrentMontageName);
	// Stop last montage and play current montage with scale and startSectionName
	void PlayMontage(ACharacter* Character, FName LastMontageName, FName CurrentMontageName, float InPlayRate, FName StartSectionName);
	// Stop play montage
	void StopMontage(ACharacter* Character, FName MontageName);
#pragma endregion

	UAnimationGameInstanceSubsystem();

#pragma region Override Function
protected:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property
#pragma region Container Object
protected:
	// Montage mepping(Key = Montage Name, value = Montage Reference)
	UPROPERTY()
	TMap<FName, class UAnimMontage*> MontageMap;
#pragma endregion
};
