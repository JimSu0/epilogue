// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "AudioGameInstanceSubsystem.generated.h"

UENUM(BlueprintType)
enum class EAudioEffect : uint8
{
	NONE = 0	UMETA(Desplayname = "None"),
	JUMP1		UMETA(Desplayname = "Jump1"),
	JUMP2		UMETA(Desplayname = "Jump2"),
	SLIDE1		UMETA(Desplayname = "Slide1"),
	SLIDE2		UMETA(Desplayname = "Slide2"),
	PICK		UMETA(Desplayname = "Pick"),
	ATTACK1		UMETA(Desplayname = "Attack1"),
	ATTACK2		UMETA(Desplayname = "Attack2"),
	HIT			UMETA(Desplayname = "Hit"),
	COLLISION	UMETA(Desplayname = "Collision"),
	FALLING		UMETA(Desplayname = "Falling"),
	SELECT		UMETA(Desplayname = "Select"),
	PRESS		UMETA(Desplayname = "Press"),
};

UENUM(BlueprintType)
enum class EAudioBGM : uint8
{
	NONE	UMETA(Desplayname = "None"),
	Train	UMETA(Desplayname = "Train"),
	ARID	UMETA(Desplayname = "ARID"),
	INTRO	UMETA(Desplayname = "Intro"),
};

UENUM(BlueprintType)
enum class EAudioKnob : uint8
{
	MASTER = 0	UMETA(Desplayname = "Master"),
	MUSIC		UMETA(Desplayname = "Music"),
	EFFECT		UMETA(Desplayname = "Effect"),
};

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAudioGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

	
// Function
public:
	// Play audio effect with enum
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void PlayAudioEffect(EAudioEffect SoundEffect);
	// Play audio background music with enum
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void PlayAudioBGM(EAudioBGM BGM);
	// Pause BGM
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void PauseBGM(bool bPause);

	void AudioKnobSelectUp();
	void AudioKnobSelectDown();
	
	EAudioKnob GetAudioKnob() const { return CurrentKnob; }

protected:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	
// Property
protected:
	UPROPERTY(BlueprintReadOnly, Category="AudioSubsystem")
	TMap<EAudioEffect, USoundWave*> SoundEffectMap;
	UPROPERTY(BlueprintReadOnly, Category="AudioSubsystem")
	TMap<EAudioBGM, USoundWave*> SoundBGMMap;

	UPROPERTY(BlueprintReadOnly, Category="AudioSubsystem")
	UAudioComponent* SoundEffectPlayer = nullptr;
	UPROPERTY(BlueprintReadOnly, Category="AudioSubsystem")
	UAudioComponent* SoundBGMPlayer = nullptr;

	// Current playing BGM
	EAudioBGM CurrentBGM = EAudioBGM::NONE;

	// Current audio volume Knob
	EAudioKnob CurrentKnob;
};
