// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "DialogueGameInstanceSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UDialogueGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
// Function

// Interface Function
public: 
	// Start Dialogue
	void StartDialogue(class UDialogueActorComponent* ActorComponent, UDialogueActorComponent* AnotherActorComponent);
	// End Dialogue
	void EndDialogue();
// Blueprint Function
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	UDialogueActorComponent* GetActorDialogueComponent() const { return ActorDialogueComponent; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	UDialogueActorComponent* GetAnotherActorDialogueComponent() const { return AnotherActorDialogueComponent; }
	
#pragma region Get & Set Function
public:
	
#pragma endregion;

#pragma region Override Function
protected:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
#pragma endregion
	
//----------------------------------------------------------------------------------------------------	
// Property
protected:
	UPROPERTY()
	UDialogueActorComponent* ActorDialogueComponent;
	UPROPERTY()
	UDialogueActorComponent* AnotherActorDialogueComponent;
};
