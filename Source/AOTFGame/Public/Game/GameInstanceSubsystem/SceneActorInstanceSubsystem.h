// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "SceneActorInstanceSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API USceneActorInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()


public:
	TMap<FString, AActor*> SceneActorMap;
};
