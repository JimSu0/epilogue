// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "WidgetGameInstanceSubsystem.generated.h"

UENUM(BlueprintType)
enum class EWidgetType : uint8
{
	NONE = 0	UMETA(Desplayname = "None"),
	LOGO		UMETA(Desplayname = "Logo"),
	RADIO		UMETA(Desplayname = "Radio"),
	DIALOGUE	UMETA(Desplayname = "Dialogue"),
};

// 移动到Dialogue脚本界面中
UENUM(BlueprintType)
enum class EDialogueWidgetSubobjectType : uint8
{
	NONE = 0			UMETA(Desplayname = "None"),
	DIALOGUE_BOX		UMETA(Desplayname = "DialogueBox"),
	DIALOGUE_OPTION		UMETA(Desplayname = "DialogueOption"),
};

/**
 * 
 */
UCLASS()
class AOTFGAME_API UWidgetGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
// Function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// Show this type widget
	void ShowWidget(EWidgetType WidgetType);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// Hide this type widget
	void HideWidget(EWidgetType WidgetType);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// Is this widget on show
	bool IsShow(EWidgetType WidgetType);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// Get widget object with Widget type
	class UAOTFUserWidget* GetWidget(EWidgetType WidgetType);
	
	/**Get the top widget
	 * Because the widget is not processed according to the weight at present, the top layer is the last displayed widget
	 */
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	UAOTFUserWidget* GetTopWidget();

protected:
	bool Contains(TArray<UAOTFUserWidget*> WidgetList, EWidgetType WidgetType);
	UAOTFUserWidget* Find(TArray<UAOTFUserWidget*> WidgetList, EWidgetType WidgetType);
	
protected:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override; 
	
// Property
protected:
	UPROPERTY(BlueprintReadOnly, Category="WidgetSubsystem")
	TMap<EWidgetType, TSubclassOf<UAOTFUserWidget>> WidgetObjectMap;

	// 移动到Dialogue脚本界面中
	UPROPERTY(BlueprintReadOnly, Category="WidgetSubsystem")
	TMap<EDialogueWidgetSubobjectType, TSubclassOf<UAOTFUserWidget>> DialogueWidgetObjectMap;
	// On display widget
	TArray<UAOTFUserWidget*> OnShowWidgetList;
};
