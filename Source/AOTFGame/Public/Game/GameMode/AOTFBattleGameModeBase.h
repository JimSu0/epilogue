// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AOTFBattleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API AAOTFBattleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
