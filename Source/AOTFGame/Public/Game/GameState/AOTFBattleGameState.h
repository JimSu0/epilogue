// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "AOTFBattleGameState.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API AAOTFBattleGameState : public AGameStateBase
{
	GENERATED_BODY()
// Fucntion
public:
	bool GetIsGamePause() const { return bGamePause; }

	void SetGamePause(bool bPause) { bGamePause = bPause; }
	
// Property
protected:
	// Is game pause
	bool bGamePause = false;
};
