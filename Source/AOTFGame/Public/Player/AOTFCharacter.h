// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Widget/Start/Dialogue/Data/FDialogueLine.h"
#include "AOTFCharacter.generated.h"

UCLASS()
class AOTFGAME_API AAOTFCharacter : public ACharacter
{
	GENERATED_BODY()
	
// Function
	
#pragma region Interface Function
public:
	// Launch player in axis Y & Z whith LaunchVectorScale, inAirScale is when character in air LaunchVectorScale's scale value 
	void LaunchYZ(FVector2D LaunchVectorScaleYZ, float inAirScale = 1.0f);
	// Launch player with LaunchForce direction
	void Launch2D(FVector LaunchForce, float LaunchCoefficient = 1);
	// Into game playing mode
	void IntoGamePlayingMode();
#pragma endregion
	
#pragma region Get & Set Function
public:
	// Get animation subsystem object
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class UAnimationGameInstanceSubsystem* GetAnimationSubsystem() const;
	// Get player controller
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class AAOTFPlayerController* GetAOTFPlayerController() const;
	// Get palyer state
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class AAOTFPlayerState* GetAOTFPlayerState() const;
	// Get cameraComponent subobject
	class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	// Get cameraSpringArm subobject
	class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	// Get the point which hook catched
	AActor* GetHookPointActor() const { return HookPointActor; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class UInteractByRayActorComponent* GetRayInteractiveComponent() const { return RayInteractComponent; }
	// Get collision interact actor
	class UInteractByKnockActorComponent* GetCollisionInteractiveComponent() const { return CollisionInteractComponent; }
	// Get dialogue component
	class UDialogueActorComponent* GetDialogueComponent() const { return PlayerDialogueComponent; }

	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr1() const { return DialogueLineArr1; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr2() const { return DialogueLineArr2; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr3() const { return DialogueLineArr3; }
	
	
	// Set the point which hook catched
	void SetHookPointActor(AActor* Point) { HookPointActor = Point; }
#pragma endregion

// Blueprint function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineDataArr();
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// 添加当前对话序列索引
	void AddDialogueLineArrIndex() { ++ DialogueLineArrIndex; }
	
#pragma region Normal Fuction
public:
	AAOTFCharacter();
protected:
	// Call back when interact
	void Interact(UInteractByRayActorComponent* Component);
#pragma endregion

#pragma region Controller Input Function
public:
	// Called for side to side input
	void MoveRight(float Value);
	// Start jump
	void StartJump();
	// Satrt slide
	void Slide();
	// Satrt dodge
	void Dodge();
	
	// Light Attack
	void LightAttackStart();
	void LightAttackEnd();
	
	// Heavy attack
	void HeavyAttack();
	// Hook point
	void Hook();
	// Pause game
	void PauseGame();
#pragma endregion
	
#pragma region Override Function
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;;
#pragma endregion
//----------------------------------------------------------------------------------------------------
// Property
#pragma region Subsystem Property
protected:
	// The ptr of animation subsystem
	UPROPERTY()
	UAnimationGameInstanceSubsystem* AnimationSubsystem;
#pragma endregion

#pragma region Reference Property
	UPROPERTY()
	AAOTFPlayerController* AOTFPlayerController;
	UPROPERTY()
	AAOTFPlayerState* AOTFPlayerState;
	// The hook can catch point actor
	UPROPERTY()
	AActor* HookPointActor;
	// Interact by ray actor
	UPROPERTY()
	UInteractByRayActorComponent* RayInteractComponent;
	// Interact by collision
	UPROPERTY()
	UInteractByKnockActorComponent* CollisionInteractComponent;
#pragma endregion
	
#pragma region Component Property
public:
	// Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* SideViewCameraComponent;
	// SpringArm
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;
	// Input buffer
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "InputBuffer", meta = (AllowPrivateAccess = "true"))
	class UInputBufferActorComponent* InputBuffer;
	// Interactive component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "InteractiveComponent", meta = (AllowPrivateAccess = "true"))
	UInteractByRayActorComponent* InteractComponent;
	// Dialogue component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "DialogueComponent", meta = (AllowPrivateAccess = "true"))
	UDialogueActorComponent* PlayerDialogueComponent;
	
#pragma endregion

#pragma region Value Property
#pragma endregion

protected:
	// 当前对话序列索引
	int32 DialogueLineArrIndex = 1;
	
	// 暂时支持两个对话数组
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr3;


protected:
	float CharacterXOffset;
};
