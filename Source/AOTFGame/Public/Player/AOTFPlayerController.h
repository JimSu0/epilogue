// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AOTFPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API AAOTFPlayerController : public APlayerController
{
	GENERATED_BODY()
// Function
#pragma region Get & Set Function
public:
	// Get widget subsystem object
	class UWidgetGameInstanceSubsystem* GetWidgetSubsystem() const;
	// Get audio subsystem object
	class UAudioGameInstanceSubsystem* GetAudioSubsystem() const;
	// Get dialogue subsystem object
	class UDialogueGameInstanceSubsystem* GetDialogueSubsystem() const;

	UFUNCTION(BlueprintCallable, Category="CPP_Fucntion")
	class AAOTFCharacter* GetAOTFCharactor() const;
	UFUNCTION(BlueprintCallable, Category="CPP_Fucntion")
	class AAOTFPlayerState* GetAOTFPlayerState() const;
	UFUNCTION(BlueprintCallable, Category="CPP_Fucntion")
	class AAOTFBattleGameState* GetAOTFBattleGameState() const;
#pragma endregion


	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void ShowWidget(enum EWidgetType WidgetType);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void HideWidget(EWidgetType WidgetType);

#pragma region Input Function
public:
	// Moveable relevant Input
	// Called for side to side input
	void MoveRight(float Value);
	// Interact with actor
	void Interact();
	// Start jump
	void Jump();
	// Stop jump
	void StopJumping();
	// Start slide
	void Slide();
	// Start dodge
	void Dodge();
	// Start light attack
	void LightAttackStart();
	// End light attack
	void LightAttackEnd();
	
	// Heavy attack
	void HeavyAttack();
	// Hook point
	void Hook();
	// Pause Game
	void PauseGame();

	// Widget control relevant Input
	// Control the volume
	void ScrollerControl(float Value);
	// Select up
	void SelectUp();
	// Select down
	void SelectDown();
	// Go next
	void GoNext();
#pragma endregion

#pragma region Override Function
protected:
	virtual void BeginPlay() override;
	// Input controller
	virtual void SetupInputComponent() override;
	// Update
	virtual void Tick(float DeltaSeconds) override;
#pragma endregion

public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void EnterInteract();
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void ExitInteract();

	UFUNCTION(BlueprintImplementableEvent, Category="Cpp_Event")
	void OnJumpDo();
	
// Property
	
// Subsystem Property
protected:
	// The ptr of widget subsystem
	UPROPERTY(BlueprintReadOnly)
	UWidgetGameInstanceSubsystem* WidgetSubsystem;
	// The ptr of audio subsystem
	UPROPERTY(BlueprintReadOnly)
	UAudioGameInstanceSubsystem* AudioSubsystem;
	// The ptr of Dialogue subsystem
	UPROPERTY(BlueprintReadOnly)
	UDialogueGameInstanceSubsystem* DialogueSubsystem;

public:
	UPROPERTY(BlueprintReadOnly)
	class USceneActorInstanceSubsystem* SceneActorSubsystem;
	
#pragma region Reference Property
protected:
	UPROPERTY()
	AAOTFCharacter* AOTFCharacter;
	UPROPERTY()
	AAOTFPlayerState* AOTFPlayerState;
	UPROPERTY()
	AAOTFBattleGameState* AOTFBattleGameState;
#pragma endregion 
};
