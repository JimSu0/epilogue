// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AOTFPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API AAOTFPlayerState : public APlayerState
{
	GENERATED_BODY()
// Function
public:
	void TurnMasterVolume(float Scale);
	void TurnMusicVolume(float Scale);
	void TurnEffectVolume(float Scale);
	
#pragma region Get & Set Function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsAir() const;
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsTwoStageJump() const { return bIsTwoStageJump; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsHandleUp() const { return bIsHandleUp; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsHandleDown() const { return bIsHandleDown; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsCanMove() const { return bCanMove; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsCanControllerMove() const { return bCanControllerMove; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsInGamePlayingMode() const { return bInGamePlayingMode; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool IsInteracted() const { return bInteracted; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class AAOTFCharacter* GetAOTFCharacter() const { return AOTFCharacter; }

	int32 GetMasterVolume() const { return CurrentMasterVolume; }
	int32 GetMusicVolume() const { return CurrentMusicVolume; }
	int32 GetEffectVolume() const { return CurrentEffectVolume; }
	
	void SetTwoStageJump(bool Is) { bIsTwoStageJump = Is; }
	void SetHandleUp(bool Is) { bIsHandleUp = Is; }
	void SetHandleDown(bool Is) { bIsHandleDown = Is; }
	void SetCanMove(bool Is) { bCanMove = Is; }  
	void SetInGamePlayingMode(bool Is);
	void SetInteracted(bool Is) { bInteracted = Is; }

	void SetMasterVolume(int32 Volume) { CurrentMasterVolume = Volume; }
	void SetMusicVolume(int32 Volume) { CurrentMusicVolume = Volume; }
	void SetEffectVolume(int32 Volume) { CurrentEffectVolume = Volume; }
	
#pragma endregion

#pragma region Override Function
protected:
	virtual void BeginPlay() override;
#pragma endregion


	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	bool GetAttack() const { return bAttack; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	int32 GetMissCount() const { return MissCount; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void LoseMissCount() { --MissCount; }
	
//----------------------------------------------------------------------------------------------------
// Property
#pragma region Input State Property
protected:
	// Is player jump two stage
	bool bIsTwoStageJump;
	// Is player handle key W
	bool bIsHandleUp;
	// Is player handle key S
	bool bIsHandleDown;
	// Is plauer can move
	bool bCanMove = true;
	// Is plauer can controller move
	bool bCanControllerMove = true;
	// Is in Game playing Mode
	bool bInGamePlayingMode = false;
	// Is in iteract state
	bool bInteracted = false;

	// Master volume
	float CurrentMasterVolume;
	// Min master volume
	float MinMasterVolume = 0.0f;
	// Max Master volume
	float MaxMasterVolume = 1.0f;
	
	
	// Music volume
	float CurrentMusicVolume;
	// Min music volume
	float MinMusicVolume = 0.0f;
	// Max music volume
	float MaxMusicVolume = 1.0f;
	
	// Effect volume
	float CurrentEffectVolume;
	// Min effect volume
	float MinEffectVolume = 0.0f;
	// Max effect volume
	float MaxEffectVolume = 1.0f;
	
#pragma endregion;

#pragma region Reference Property
protected:
	UPROPERTY()
	AAOTFCharacter* AOTFCharacter;
#pragma endregion;


public:
	// 是否与阿底提交互过
	bool bAditi = false;
	// 是否与机器人交互过
	bool bRobot = false;
	
	// 是否正在攻击
	bool bAttack = false;

	// 失误机会
	int32 MissCount = 10;
};
