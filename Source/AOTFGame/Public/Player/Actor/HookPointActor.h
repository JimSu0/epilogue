// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HookPointActor.generated.h"

UCLASS()
class AOTFGAME_API AHookPointActor : public AActor
{
	GENERATED_BODY()
	
// Function
#pragma region Get & Set Function
	class UInteractByKnockActorComponent* GetInteractiveComponent() const { return InteractComponent; }
#pragma endregion
	
#pragma region Normal Function	
public:	
	AHookPointActor();
protected:
	void Interact(AActor* InteractActor);
#pragma endregion

#pragma region Override Function
protected:
	virtual void BeginPlay() override;
#pragma endregion
	
//----------------------------------------------------------------------------------------------------
// Property
#pragma region Reference Property
protected:
	UPROPERTY()
	class AAOTFCharacter* AOTFCharacter;
#pragma endregion

#pragma region Component Property
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;
	
	// TODO::Effect Object
	
	UPROPERTY(BlueprintReadOnly, Category = "InteractiveComponent", meta = (AllowPrivateAccess = "true"))
	UInteractByKnockActorComponent* InteractComponent;
#pragma endregion
};
