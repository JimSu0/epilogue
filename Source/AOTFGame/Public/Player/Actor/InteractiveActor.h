// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Player/Widget/Start/Dialogue/Data/FDialogueLine.h"
#include "InteractiveActor.generated.h"

UCLASS()
class AOTFGAME_API AInteractiveActor : public AActor
{
	GENERATED_BODY()

// Function
public:
	class UInteractByRayActorComponent* GetInteractComponent() const;
	class UDialogueActorComponent* GetDialogueComponent() const;
	
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr1() const { return DialogueLineArr1; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr2() const { return DialogueLineArr2; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr3() const { return DialogueLineArr3; }

// Blueprint function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineDataArr();
	// 添加当前对话序列索引
	void AddDialogueLineArrIndex() { ++ DialogueLineArrIndex; }
	
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void Interact(UInteractByRayActorComponent* InteractActor);
	// On inter interact call
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void InterInteract(class AAOTFPlayerController* AOTFController);
	// On exit interact call
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void ExitInteract(AAOTFPlayerController* AOTFController);
	
public:
	AInteractiveActor();
protected:
	virtual void BeginPlay() override;

// Property
public:
	// Interactive component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "InteractiveComponent", meta = (AllowPrivateAccess = "true"))
	UInteractByRayActorComponent* InteractComponent;
	// Dialogue component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DialogueComponent", meta = (AllowPrivateAccess = "true"))
	UDialogueActorComponent* DialogueComponent;
	// Actor mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="InteractiveActor")
	UStaticMeshComponent* ActorMesh;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="InteractiveActor")
	class UBoxComponent* BoxCollider;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="InteractiveActor")
	FVector RayEndPosition;


	int32 DialogueLineArrIndex = 1;
	// 暂时支持三个对话数组
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr3;
};
