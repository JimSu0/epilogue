// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ModeChangeActor.generated.h"

UCLASS()
class AOTFGAME_API AModeChangeActor : public AActor
{
	GENERATED_BODY()
	
// Function
public:
	AModeChangeActor();
protected:
	UFUNCTION()
	void OverlapBegins(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                   int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
protected:
	virtual void BeginPlay() override;
	
// Property
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ModeChangeActor")
	class UBoxComponent* BoxCollider;

	// Is changed mode
	bool bChanged = false;
};
