// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MoveableActor.generated.h"

UCLASS()
class AOTFGAME_API AMoveableActor : public AActor
{
	GENERATED_BODY()
// Function
#pragma region Bluieprint Function
public:
	UFUNCTION(BlueprintImplementableEvent,Category="CPP_Function")
	void Move(UStaticMeshComponent* Mesh);
#pragma endregion

#pragma region Normal Function
public:
	AMoveableActor();
#pragma endregion
	
#pragma region Override Function
protected:
	virtual void BeginPlay() override;
#pragma endregion

public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void Register(FString ActorName);
	

//----------------------------------------------------------------------------------------------------
// Property
#pragma region Component Property
public:
	UPROPERTY(BlueprintReadOnly, Category="MoveableActor")
	class UBoxComponent* BoxComponent;
	// NPC character object, set in blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="MoveableActor")
	UStaticMeshComponent* ActorMesh;
#pragma endregion
	
#pragma region Value Property
protected:
	
#pragma endregion
	
#pragma region Reference Property
protected:
	// NPC character object, set in blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="NPC")
	class AAOTFNPCCharacter* NPCCharacter;
#pragma endregion
};
