// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Player/Widget/Start/Dialogue/Data/FDialogueLine.h"
#include "DialogueActorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AOTFGAME_API UDialogueActorComponent : public UActorComponent
{
	GENERATED_BODY()

// Function
public:
	void Init(AActor* Owner);

public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	int32 GetIdentificationStr() const { return IdentificationNum; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void SetIdentificationStr(int32 Num) { IdentificationNum = Num; }
	
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	FString GetSelfName() const { return SelfName; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	UTexture2D* GetSelfIcon() const { return SelfIconTexture; }

	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr();
public:
	UDialogueActorComponent();
protected:
	virtual void BeginPlay() override;

// Property
public:
	UPROPERTY(BlueprintReadOnly, Category="DialogueComponent")
	AActor* OwnerActor;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	FString SelfName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	UTexture2D* SelfIconTexture;
	UPROPERTY(BlueprintReadOnly ,Category="DialogueComponent")
	int32 IdentificationNum;
};
