// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InputBufferActorComponent.generated.h"

// The type of input Buffer
UENUM(BlueprintType)
enum EInputType
{
	EMPTY = 0			UMETA(DisplayName = "Empty"),
	// Weapon
	TOGGLE_WEAPON = 1	UMETA(DisplayName = "ToggleWeapon"),
	// Attack
	LIGHT_ATTACK = 2	UMETA(DisplayName = "LightAttack"),
	HEAVY_ATTACK = 3	UMETA(DisplayName = "HeavyAttack"),
	HOOK = 4			UMETA(DisplayName = "Hook"),
	// Move
	SLIDE = 5			UMETA(DisplayName = "Slide"),
	DODGE = 6			UMETA(DisplayName = "Dodge"),
	JUMP = 7			UMETA(DisplayName = "Jump"),
	MOVE_UP = 8			UMETA(DisplayName = "MoveUp"),
	MOVE_DOWN = 9		UMETA(DisplayName = "MoveDown")
};

// Input Conrtroller
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AOTFGAME_API UInputBufferActorComponent : public UActorComponent
{
	GENERATED_BODY()
//----------------------------------------------------------------------------------------------------
// Function
#pragma region Interface Function
public:
	// Initlized component
	void Init(class AAOTFCharacter* Character, class AAOTFPlayerState* PlayerState);
	// Add an input type into buffer
	void Input(EInputType KeyType);
	// Add one element to light attack key list
	void AddLightAttackList(class FInputKey* Key) { StoredLightAttackKeyList.Add(Key); }
	// Clean up light attack list
	void EmptyLightAttackKeyList() { StoredLightAttackKeyList.Empty(); }
	// Find light attack object in light attack list with index
	FInputKey* FindObjectInLightAttackKeyList(int32 index) { return StoredLightAttackKeyList[index]; }
	// Add one element to input key list
	void AddKeyList(FInputKey* Key) { StoredKeyList.Add(Key); }
	// Add one element to combo list
	void AddComboList(FInputKey* Key) { StoredComboKeyList.Add(Key); }
#pragma endregion

#pragma region Get & Set Function
public:
	// ComboCheckFrameCount
	int32 GetComboCheckFrameCount() const { return ComboCheckFrameCount; }
	// LightAttackMaxCount
	int32 GetLightAttackMaxCount() const { return LightAttackMaxCount; }
	// AOTFCharacter
	AAOTFCharacter* GetAOTFCharacter() const;
	// AOTFPlayerState
	AAOTFPlayerState* GetAOTFPlayerState() const;
	// StoredKeyList
	TArray<FInputKey*> GetStoredKeyList() const { return StoredKeyList; }
	// StoredComboKeyList
	TArray<FInputKey*> GetComboKeyList() const { return StoredComboKeyList; }
	// StoredLightAttackKeyList
	TArray<FInputKey*> GetStoredLightAttackKeyList() const { return StoredLightAttackKeyList; }
	// StoredLightAttackKeyList.Num()
	int32 GetLightAttackKeyListNum() const { return StoredLightAttackKeyList.Num(); }
#pragma endregion
	
#pragma region Normal Function
public:	
	UInputBufferActorComponent();
	FInputKey* GenerateInputKey(EInputType Type);
#pragma endregion

#pragma region Override Function
protected:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property
#pragma region Const Property
protected:
	// Max check combo input count
	const int32 ComboCheckFrameCount = 3;
	// Max light attack count
	const int32 LightAttackMaxCount = 3;
#pragma endregion

#pragma region Reference Property
protected:
	// The ptr of pawn which owns this component
	UPROPERTY(BlueprintReadOnly)
	AAOTFCharacter* AOTFCharacter;
	// Current possess character player state
	UPROPERTY(BlueprintReadOnly)
	AAOTFPlayerState* AOTFPlayerState;
#pragma endregion
	
#pragma region Container Property
	TMap<EInputType, bool> KeyStateMap;
	// Input key list
	TArray<FInputKey*> StoredKeyList;
	// The key which Characteristic is COMBINATION
	TArray<FInputKey*> StoredComboKeyList;
	// The key which input type is LIGHT_ATTACK
	TArray<FInputKey*> StoredLightAttackKeyList;
#pragma endregion
	
#pragma region Value Property
protected:
	// Can put input key into Buffer
	bool bCanBufferInput = false;
	// Is first break input key
	bool bCanBreakTypeKeyInput = true;
	// Is first combo input key
	bool bFirstComboInputKey = true;
	// Is play combo montage
	bool bPlayCombo = false;
	// Is start calculation framecount
	bool bStartCalcFrame = false;
#pragma endregion
};
