// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputKey.h"

/**
 * Dodge key class
 */
class AOTFGAME_API FDodgeKey : FInputKey
{
// Function
#pragma region Normal Function
public:
	FDodgeKey(UInputBufferActorComponent* InputBufferActorComponent);
	virtual ~FDodgeKey() override;
#pragma endregion

#pragma region Normal Function
protected:
	virtual void OnInputDo(bool& bCanInput) override;
	virtual void OnChillDownDo() override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property

#pragma region Value Property
protected:
	// The multiple of axis Y launch velocity when dodge 
	int32 DodgeLaunchVelocityYMultiple;
	// The velocity of axis Z launch velocity
	float LaunchVelocityZ;
	// TODO :: Call back on chill down finished
#pragma endregion
};
