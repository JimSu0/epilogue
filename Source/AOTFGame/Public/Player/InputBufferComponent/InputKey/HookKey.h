// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputKey.h"

/**
 * Dodge key class
 */
class AOTFGAME_API FHookKey : FInputKey
{
// Function
#pragma region Normal Function
public:
	FHookKey(UInputBufferActorComponent* InputBufferActorComponent);
	virtual ~FHookKey() override;
#pragma endregion

#pragma region Normal Function
protected:
	virtual void OnInputDo(bool& bCanInput) override;
	virtual void OnChillDownDo() override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property

#pragma region Value Property
protected:
	// The coefficient of launch character
	float LaunchCoefficient;
	// TODO :: Call back on chill down finished
#pragma endregion
};
