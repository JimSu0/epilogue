// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Input key class
 */
class AOTFGAME_API FInputKey
{
//----------------------------------------------------------------------------------------------------
// Function
#pragma region Interface Function
public:
	// Input this key
	void Input(bool& bCanInput);
	// Calculation colling tick
	bool ChillDownTick(bool& bCanInput);
	// Direct end cooling
	void ChillDownEnd(bool& bCanInput);
#pragma endregion 
#pragma region Get Function
public:
	enum EInputType GetInputType() const { return InputType; }
	FName GetCharacteristic() const { return Characteristic; }
	FName GetMontageName() const { return MontageName; }
	int32 GetChillDownFrame() const { return ChillDownFrame; }
#pragma endregion
	
#pragma region Normal Function
public:
	FInputKey(class UInputBufferActorComponent* InputBufferActorComponent);
	virtual ~FInputKey() {};
#pragma endregion

#pragma region Virtual Function
protected:
	// When key press do
	virtual void OnInputDo(bool& bCanInput) { };
	// When cooling is over
	virtual void OnChillDownDo() { };
	// Executes at the specified number of frames(Run before OnChillDownDo())
	virtual void OnSpecifiedFrameDo() { };
#pragma endregion
	
//----------------------------------------------------------------------------------------------------
// Property
#pragma region Reference Property
protected:
	// Input buffer
	UInputBufferActorComponent* InputBuffer;
#pragma endregion 

#pragma region Value Property
protected:
	// The type of this inputType
	EInputType InputType;
	// The characteristic this input 
	FName Characteristic;
	// The name of the montage for this input mapping
	FName MontageName;
	// The number of frame to ready this input opreation
	int32 ChillDownFrame;
	// Current frame count
	int32 CurrentFrame;
	// Specified frame count(Must small or equal then ChillDownFrame)
	int32 SpecifiedFrame;
#pragma endregion 
};
