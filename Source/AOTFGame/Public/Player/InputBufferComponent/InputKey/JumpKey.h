// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputKey.h"

/**
 * Jump key class
 */
class AOTFGAME_API FJumpKey : FInputKey
{
// Function
#pragma region Normal Function
public:
	FJumpKey(class UInputBufferActorComponent* InputBufferActorComponent);
	virtual ~FJumpKey() override;
#pragma endregion

#pragma region Normal Function
protected:
	virtual void OnInputDo(bool& bCanInput) override;
	virtual void OnChillDownDo() override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property
#pragma region Reference Property
protected:
	// TODO :: Call back on chill down finished
#pragma endregion
};
