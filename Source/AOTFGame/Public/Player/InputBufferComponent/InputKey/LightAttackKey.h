// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputKey.h"

// Once attack finish call back
DECLARE_DELEGATE(OnLightAttackEndCallBack);

/**
 * Light attack key class
 */
class AOTFGAME_API FLightAttackKey : FInputKey
{
//----------------------------------------------------------------------------------------------------
// Function
#pragma region Normal Function
public:
	FLightAttackKey(UInputBufferActorComponent* InputBufferActorComponent);
	virtual ~FLightAttackKey() override;
private:
	// Can input light attack
	bool CanInput();
	// Get self index at light attack list(key = Is in this list, value = inedx)
	TKeyValuePair<bool, int32> GetIndex();
#pragma endregion

#pragma region Normal Function
protected:
	virtual void OnInputDo(bool& bCanInput) override;
	virtual void OnChillDownDo() override;
	virtual void OnSpecifiedFrameDo() override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property
#pragma region Value Property
protected:
	// Is this key next key is light attack key
	bool HasNextLihtAttack; 
#pragma endregion
	
#pragma region Delegate Property
public:
	OnLightAttackEndCallBack LightAttackEndCallBack;
#pragma endregion

#pragma region Reference Property
	FInputKey* LastComboKey;
#pragma endregion
};
