// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputKey.h"

/**
 * Input key class
 */
class AOTFGAME_API FSlideKey : FInputKey
{
// Function
#pragma region Normal Function
public:
	FSlideKey(UInputBufferActorComponent* InputBufferActorComponent);
	virtual ~FSlideKey() override;
#pragma endregion

#pragma region Normal Function
protected:
	virtual void OnInputDo(bool& bCanInput) override;
	virtual void OnChillDownDo() override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property

#pragma region Value Property
protected:
	// The multiple of axis Y launch velocity when slide 
	int32 SlideLaunchVelocityYMultiple;
	// The velocity of axis Z launch velocity
	float LaunchVelocityZ;
	// TODO :: Call back on chill down finished
#pragma endregion
};
