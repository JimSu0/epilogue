// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractByKnockActorComponent.generated.h"

DECLARE_DELEGATE_OneParam(OnCollisionInteractCallback, AActor*);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AOTFGAME_API UInteractByKnockActorComponent : public UActorComponent
{
	GENERATED_BODY()

// Function
#pragma region Interface Function
public:
	void Init(AActor* Owner, TFunction<void(AActor*)> Callback);
#pragma endregion

#pragma region Get& Set Function
public:
	AActor* GetOwnerActor() const { return OwnerActor; }
	class USphereComponent* GetSphereTrigger() const { return SphereTrigger; }
#pragma endregion
	
#pragma region Normal Fubction
public:	
	UInteractByKnockActorComponent();
protected:
	UFUNCTION()
	// On sphere trigger enter call
	void Interact(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                 int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	// On sphere trigger out call
	void EndInteract(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                 int32 OtherBodyIndex);
#pragma endregion

#pragma region Override Function
protected:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion


//----------------------------------------------------------------------------------------------------
// Property
#pragma region Component Property
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Trigger")
	class USphereComponent* SphereTrigger;
#pragma endregion
	
#pragma region Delegate Property
public:
	OnCollisionInteractCallback CollisionInteractCallback;
#pragma endregion

#pragma region Reference Property
protected:
	AActor* OwnerActor;
#pragma endregion
};