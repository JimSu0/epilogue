// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractByRayActorComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(OnRayInteractCallback, class UInteractByRayActorComponent*);
DECLARE_MULTICAST_DELEGATE_OneParam(OnRayInterInteractCallback, class AAOTFPlayerController*);
DECLARE_MULTICAST_DELEGATE_OneParam(OnRayExitInteractCallback, AAOTFPlayerController*);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AOTFGAME_API UInteractByRayActorComponent : public UActorComponent
{
	GENERATED_BODY()

// Function
#pragma region Interface Function
public:
	// When owner is Actor
	void Init(AActor* Owner, FVector RayCastEndPosition,
	          TFunction<void(UInteractByRayActorComponent*)> Component = nullptr,
	          TFunction<void(class AAOTFPlayerController*)> InterCallback = nullptr,
	          TFunction<void(AAOTFPlayerController*)> ExitCallback = nullptr);
	// When inter interact do
	void InterInteract(AAOTFPlayerController* AOTFController);
	// When exit interact do
	void ExitInteract(AAOTFPlayerController* AOTFController);
#pragma endregion

#pragma region Get & Set Function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	AActor* GetOwnerActor() const { return OwnerActor; };
#pragma endregion

#pragma region Blueprint Function
	
#pragma endregion
	
#pragma region Normal Function
public:	
	UInteractByRayActorComponent();
protected:
	// When interact do
	void Interact(AActor* InteractActor);
#pragma endregion

#pragma region Override Function
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
private:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion
//----------------------------------------------------------------------------------------------------
// Property

#pragma region Component Property
protected:
#pragma endregion

#pragma region Delegate Property
public:
	// On interact call 
	OnRayInteractCallback OnRayInteractive;
	// On inter interact call
	OnRayInterInteractCallback OnRayInterInteractive;
	// On exit interact call
	OnRayExitInteractCallback OnRayExitInteractive;
#pragma endregion

#pragma region Reference Property
protected:
	UPROPERTY(BlueprintReadWrite, Category = "OwnerActor")
	AActor* OwnerActor;
	UPROPERTY()
	AActor* CurrentInteractiveActor;
#pragma endregion

#pragma region Value Property
protected:
	// The end point's location
	FVector RayEndPosition;
#pragma endregion
};
