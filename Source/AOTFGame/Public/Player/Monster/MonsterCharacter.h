// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MonsterCharacter.generated.h"

UCLASS()
class AOTFGAME_API AMonsterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMonsterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


public:
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void Interact(class UInteractByRayActorComponent* Component);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void InterInteract(class AAOTFPlayerController* AOTFController);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void ExitInteract(AAOTFPlayerController* AOTFController);
	

public:
	UPROPERTY(BlueprintReadOnly)
	class UInteractByRayActorComponent* InteractComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="MonsterCharacter")
	float DirectLength;
};
