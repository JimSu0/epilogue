// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/Character.h"
#include "Player/Widget/Start/Dialogue/Data/FDialogueLine.h"
#include "AOTFNPCCharacter.generated.h"

UCLASS()
class AOTFGAME_API AAOTFNPCCharacter : public ACharacter
{
	GENERATED_BODY()
	
// Function
#pragma region Get & Set Function
public:
	class UInteractByRayActorComponent* GetInteractComponent() const { return InteractComponent; }
	class UDialogueActorComponent* GetDialogueComponent() const { return DialogueComponent; }

	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr1() const { return DialogueLineArr1; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr2() const { return DialogueLineArr2; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineArr3() const { return DialogueLineArr3; }
#pragma endregion

#pragma region Blueprint Function
public:
	// Blueprint function
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	TArray<FDialogueLine> GetDialogueLineDataArr();
	// 添加当前对话序列索引
	void AddDialogueLineArrIndex() { ++ DialogueLineArrIndex; }
	
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void Interact(UInteractByRayActorComponent* InteractActor);
	// On inter interact call
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void InterInteract(class AAOTFPlayerController* AOTFController);
	// On exit interact call
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void ExitInteract(AAOTFPlayerController* AOTFController);
#pragma endregion
	
#pragma region Normal Function
public:
	// Sets default values for this character's properties
	AAOTFNPCCharacter();
#pragma endregion

#pragma region Override Function
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
#pragma endregion

//----------------------------------------------------------------------------------------------------
// Property
#pragma region Component Property
protected:
	// Interactive component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "InteractiveComponent", meta = (AllowPrivateAccess = "true"))
	UInteractByRayActorComponent* InteractComponent;
	// Delegate component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DialogueComponent", meta = (AllowPrivateAccess = "true"))
	UDialogueActorComponent* DialogueComponent;
#pragma endregion

protected:
	int32 DialogueLineArrIndex = 1;
	
	// 暂时支持三个对话数组
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueComponent")
	TArray<FDialogueLine> DialogueLineArr3;
};
