// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Game/GameInstanceSubsystem/WidgetGameInstanceSubsystem.h"

#include "AOTFUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAOTFUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
// Function
public:
	EWidgetType GetWidgetType() const { return WidgetType; };
	
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void Show(UAOTFUserWidget* SelfObj);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void Hide(UAOTFUserWidget* SelfObj);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void OnScrollerDo(float Value);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void OnSelectDo(bool bUp);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void OnGoNextDo();
	
// Property
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AOTFUserWidget")
	EWidgetType WidgetType;
};
