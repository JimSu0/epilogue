// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Dialogue/Data/FDialogueLine.h"
#include "Player/Widget/AOTFUserWidget.h"
#include "AOTFDialogueUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAOTFDialogueUserWidget : public UAOTFUserWidget
{
	GENERATED_BODY()
// Function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class UDialogueGameInstanceSubsystem* GetDialogueSubsystem() const ;
};
