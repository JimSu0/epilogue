// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/Widget/AOTFUserWidget.h"
#include "AOTFRadioUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAOTFRadioUserWidget : public UAOTFUserWidget
{
	GENERATED_BODY()

	friend class UImage;
	friend class UTextBlock;
	
// Function
public:
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void RefreshVolume();
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void RefreshKnobSelected();
protected:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void RefreshImageKnob(UImage* MasterKnob,UTextBlock* MasterVolume,
						  UImage* MusicKnob, UTextBlock* MusicVolume,
						  UImage* EffectKnob, UTextBlock* EffectVolume);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void RefreshImageSelected(UImage* MasterKnobSelected, UImage* MusicKnobSelected, UImage* EffectKnobSelected);
	
// Property
protected:
	class UAudioGameInstanceSubsystem* AudioSubsystem;
};