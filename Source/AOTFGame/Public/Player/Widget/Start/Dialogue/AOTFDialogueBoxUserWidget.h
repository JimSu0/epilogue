// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Data/FDialogueLine.h"
#include "Temporary/AditiDialogueComponent.h"
#include "Temporary/RobotDialogueComponent.h"
#include "AOTFDialogueBoxUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAOTFDialogueBoxUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
// Function
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void Init(class UDialogueActorComponent* Component);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void SetData(UDialogueActorComponent* Component);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	UDialogueActorComponent* GetDialogueComponent() const { return DialogueComponent; }
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	FDialogueLine GetCurrentLineData();
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void AddLineIndex() { ++LineIndex; }

	// Aditi
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void FunctionInitData(FLineData Data);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void InitData(FLineData Data);

	// Robot
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void FunctionInitRobotData(FRobotLineData Data);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void InitRobotData(FRobotLineData Data);
	
// Property
public:
	UPROPERTY(BlueprintReadOnly)
	UDialogueActorComponent* DialogueComponent;
	UPROPERTY(BlueprintReadOnly)
	int32 LineIndex;
	UPROPERTY(BlueprintReadOnly)
	FDialogueLine CurrentLineData;
};
