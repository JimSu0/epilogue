// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Data/FDialogueLine.h"
#include "Temporary/AditiDialogueComponent.h"
#include "Temporary/RobotDialogueComponent.h"
#include "AOTFDialogueOptionUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class AOTFGAME_API UAOTFDialogueOptionUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
// Function
public:
	
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void InitAditi(FLineData Data);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void RefreshAditi(FLineData Data);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	class UDialogueGameInstanceSubsystem* GetDialogueSubsystem() const;

	
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void SetAditiLineData(FLineData Data);

	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void InitRobot(FRobotLineData Data, int32 Index);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void RefreshRobot(FRobotLineData Data, int32 Index);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void SetRobotLineData(FRobotLineData Data, int32 Index);


	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void Init(FDialogueLine Data, FName Line, bool CanSelected);
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void SetSelected(class UImage* SelectImage, bool bIs);
	UFUNCTION(BlueprintImplementableEvent, Category="CPP_Event")
	void SetData(FDialogueLine Data, FName Line, bool CanSelected);

protected:
	UPROPERTY(BlueprintReadOnly)
	FDialogueLine LineData;
	UPROPERTY(BlueprintReadOnly)
	FName LineStr;
	UPROPERTY(BlueprintReadOnly)
	bool bCanSelected;
};
