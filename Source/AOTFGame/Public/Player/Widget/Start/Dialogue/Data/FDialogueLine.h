// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FDialogueLine.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct AOTFGAME_API FDialogueLine
{ 
	GENERATED_BODY()

// Function
public:
	FDialogueLine();
	~FDialogueLine();
	
// Property
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueLine")
	FString Mark;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueLine")
	TArray<FString> OptionArr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category="DialogueLine")
	bool bAnother = true;
};
