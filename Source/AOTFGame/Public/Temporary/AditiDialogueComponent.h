// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AditiDialogueComponent.generated.h"

// 台词数据枚举
USTRUCT(BlueprintType)
struct AOTFGAME_API FLineData
{
	GENERATED_BODY()


	FLineData()
	{
		
	}
	FLineData(FString ActorNameStr)
	{
		ActorName = ActorNameStr;
	}
	FLineData(FString ActorNameStr, class UTexture2D* Icon, FString LineStr, bool bSide)
	{
		ActorName = ActorNameStr;
		ActorIcon = Icon;
		Line = LineStr;
		bUpLine = bSide;
	}
	~FLineData()
	{
		
	}
	
	// 名称
	UPROPERTY(BlueprintReadOnly)
	FString ActorName;
	// 头像
	UPROPERTY(BlueprintReadOnly)
	UTexture2D* ActorIcon; 
	// 台词
	UPROPERTY(BlueprintReadOnly)
	FString Line;
	// 上侧还是下侧对话框
	UPROPERTY(BlueprintReadOnly)
	bool bUpLine;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AOTFGAME_API UAditiDialogueComponent : public UActorComponent
{
	GENERATED_BODY()
// Function 
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// 初始化组件
	void Init(AActor* Actor);

	// 开始对话
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	FLineData DialogueStart();
	// 下一条对话
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	FLineData GoNext();

	// 台词映射(Key = 台词, value = 上侧还是下侧对话框)
	UPROPERTY(BlueprintReadOnly)
	TArray<FLineData> LineDataArr;
	// 当前台词下标
	int32 Index = 0;
	// 是否已经开始对话
	bool bStart;

	// 对话者名
	FString Aditi = TEXT("阿底提");
	FString Player = TEXT("集川");
	// 对话者头像图片


	// 拥有者
	AActor* OwnerActor;
};
