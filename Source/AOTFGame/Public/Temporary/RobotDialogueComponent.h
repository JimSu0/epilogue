// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RobotDialogueComponent.generated.h"

// 台词数据枚举
USTRUCT(BlueprintType)
struct AOTFGAME_API FRobotLineData
{
	GENERATED_BODY()


	FRobotLineData()
	{
		
	}
	FRobotLineData(FString ActorNameStr)
	{
		ActorName = ActorNameStr;
	}
	FRobotLineData(FString ActorNameStr, class UTexture2D* Icon, TArray<FString> LineStrArr, bool bSide)
	{
		ActorName = ActorNameStr;
		ActorIcon = Icon;
		LineArr = LineStrArr;
		bUpLine = bSide;
	}
	~FRobotLineData()
	{
		
	}
	
	// 名称
	UPROPERTY(BlueprintReadOnly)
	FString ActorName;
	// 头像
	UPROPERTY(BlueprintReadOnly)
	UTexture2D* ActorIcon;
	// 台词
	UPROPERTY(BlueprintReadOnly)
	TArray<FString> LineArr;
	// 上侧还是下侧对话框
	UPROPERTY(BlueprintReadOnly)
	bool bUpLine;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AOTFGAME_API URobotDialogueComponent : public UActorComponent
{
	GENERATED_BODY()

	// Function 
public:
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	// 初始化组件
	void Init(AActor* Actor);

	// 开始对话
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	FRobotLineData DialogueStart();
	// 下一条对话
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	FRobotLineData GoNext();
	// 设置当前选中的按键
	UFUNCTION(BlueprintCallable, Category="CPP_Function")
	void SetCurrentSelected(FString SelectStr) { CurrentSelect = SelectStr; }
	
	// 复原对话数据
	FRobotLineData ResetLineData();

	// 台词映射(Key = 台词, value = 上侧还是下侧对话框)
	UPROPERTY(BlueprintReadOnly)
	TArray<FRobotLineData> LineDataArr1;
	UPROPERTY(BlueprintReadOnly)
	TArray<FRobotLineData> LineDataArr2;
	UPROPERTY(BlueprintReadOnly)
	TArray<FRobotLineData> LineDataArr3;
	UPROPERTY(BlueprintReadOnly)
	TArray<FRobotLineData> LineDataArr4;
	UPROPERTY(BlueprintReadOnly)
	TArray<FRobotLineData> LineDataArr5;
	UPROPERTY(BlueprintReadOnly)
	TArray<FRobotLineData> LineDataArr6;
	// 当前台词下标
	int32 Index = 0;
	// 是否已经开始对话
	bool bStart;
	// 常规对话是否已经执行过
	bool bDefultDialogue = false;
	// 是否进行过选择
	bool bSelected = false;
	// 门是否已经打开
	bool bDoorOpen = false;

	// 当前对话数组
	FString CurrentArr;

	// 当前选中的按键
	FString CurrentSelect = TEXT("Yes");
	
	// 对话者名
	FString Aditi = TEXT("阿底提");
	FString Robot = TEXT("编号-002");
	FString Player = TEXT("集川");
	// 对话者头像图片


	// 拥有者
	AActor* OwnerActor;
};
