	// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "StreamingLoadActor.generated.h"

	UCLASS()
class AOTFGAME_API AStreamingLoadActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStreamingLoadActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UFUNCTION()
	void OverlapBegins(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=( AllowPrivateAccess = "true"))
	UBoxComponent* OverlapVolum;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=( AllowPrivateAccess = "true"))
	FName LevelToLoad;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=( AllowPrivateAccess = "true"))
	FName LevelToUnLoad;
};
